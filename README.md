Le projet est en ligne publiquement dans un objectif de **démonstration**:
**Le code peut être buildé chez tout le monde mais l'utilisation de l'application sera limitée par le SDK Spotify.**
Le SDK Spotify requiert d'ajouter sur la console du projet les fingerprint de tous les keystores utilisés pour builder l'application, si un keystore inconnu builde l'application, les communications avec Spotify ne seront pas autorisées.
L'application reste utilisable car en cas d'erreur spotify, une alarme système prend le relais.

**Un apk buildé avec mon keystore est disponible à la racine du projet (Aube/apk_test_debug.apk)** pour pouvoir utiliser l'application avec toutes ses fonctionnalités.

Je rappelle que l'application n'est pas disponible au grand public sur le PlayStore parce qu'elle est **interdite** par les [Developer Terms of Service de Spotify](https://developer.spotify.com/terms/)