package com.chazalon.dorian.aube.common

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.internal.util.Checks
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.ui.activity.MainActivity
import com.chazalon.dorian.aube.ui.adapter.AlarmListAdapter
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
open class EspressoTests {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun openCreateFragment() {
        onView(withId(R.id.addAlarm)).perform(click())
        onView(withId(R.id.createParent)).check(matches(isDisplayed()))
    }

    @Test
    fun openEditFragment() {
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<AlarmListAdapter.Companion.AlarmViewHolder>(0, click())
        )
        onView(withId(R.id.editParent)).check(matches(isDisplayed()))
    }

    @Test
    fun openSettingsFragment() {
        onView(withId(R.id.menu_settings)).perform(click())
        onView(withId(R.id.settingsParent)).check(matches(isDisplayed()))
    }

    /**
     * https://stackoverflow.com/a/30073528
     */
    protected fun childAt(parentMatcher: Matcher<View>, position: Int): Matcher<View> =
        object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("with $position child view of type parentMatcher")
            }

            override fun matchesSafely(view: View): Boolean {
                if (view.parent !is ViewGroup) {
                    return parentMatcher.matches(view.parent)
                }
                val group = view.parent as ViewGroup
                return parentMatcher.matches(view.parent) && group.getChildAt(position) == view
            }
        }

    protected fun getResourceString(id: Int): String? = (ApplicationProvider.getApplicationContext() as Context).resources.getString(id)
    protected fun getResourceString(id: Int, vararg args: Any): String? = (ApplicationProvider.getApplicationContext() as Context).resources.getString(id, *args)

    protected fun withBackgroundColor(@ColorInt color: Int): Matcher<View> {
        Checks.checkNotNull(color)
        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description?) {
                description?.appendText("Check if color $color matches View background")
            }

            override fun matchesSafely(item: View?): Boolean {
                val backgroundColor = item?.background as ColorDrawable
                val colorDrawable = ColorDrawable(ContextCompat.getColor(item.context, color))
                return colorDrawable.color == backgroundColor.color
            }

        }
    }
}