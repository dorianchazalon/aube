package com.chazalon.dorian.aube

import android.widget.TimePicker
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.chazalon.dorian.aube.common.EspressoTests
import com.chazalon.dorian.aube.ui.adapter.PlaylistListAdapter
import org.hamcrest.Matchers
import org.hamcrest.Matchers.*
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.random.Random

@RunWith(AndroidJUnit4::class)
@LargeTest
class ConnectedTests: EspressoTests() {
    @Test
    fun createAlarmWhenConnected() {
        // Création avec formulaire vide
        onView(withId(R.id.addAlarm)).perform(click())
        onView(withId(R.id.validate)).perform(click())
        onView(withText(R.string.error_missing_at_least_one_day))
            .check(matches(isDisplayed())) // Message d'erreur des jours manquants en snackbar
        onView(allOf(withId(R.id.title), withHint(R.string.title)))
            .check(matches(hasErrorText(getResourceString(R.string.error_missing_title)))) // Message d'erreur sur le titre

        // Création avec juste un titre
        val titleText = "Test ${Random.nextInt()}"
        onView(allOf(withId(R.id.title), withHint(R.string.title))).perform(typeText(titleText))
        onView(allOf(withId(R.id.title), withText(titleText)))
            .check(matches(not(hasErrorText(getResourceString(R.string.error_missing_title))))) // Message d'erreur sur le titre retiré après avoir écrit
        onView(withId(R.id.validate)).perform(click())
        onView(withText(R.string.error_missing_at_least_one_day))
            .check(matches(isDisplayed())) // Message d'erreur des jours manquants en snackbar

        // Modification de l'heure
        val hours = 12
        val minutes = 30
        val timeText = getResourceString(R.string.hour_h, hours, minutes)
        onView(withId(R.id.time)).perform(click())
        onView(withClassName(equalTo(TimePicker::class.java.name))).check(matches(isDisplayed()))
        onView(withClassName(equalTo(TimePicker::class.java.name))).perform(PickerActions.setTime(hours, minutes))
        onView(withText("OK")).perform(click())
        onView(withId(R.id.time)).check(matches(withText(timeText)))

        // Sélection des jours
        onView(withId(R.id.monday)).perform(click())
        onView(withId(R.id.monday)).check(matches(isChecked())) // Sélection
        onView(withId(R.id.tuesday)).perform(click())
        onView(withId(R.id.tuesday)).check(matches(isChecked())) // Sélection
        onView(withId(R.id.monday)).check(matches(isChecked())) // Vérification que le premier jour n'a pas été désélectionné
        onView(withId(R.id.tuesday)).perform(click())
        onView(withId(R.id.tuesday)).check(matches(isNotChecked())) // Désélection
        onView(withId(R.id.monday)).check(matches(isChecked())) // Vérification que le premier jour n'a pas été désélectionné

        // Modification du volume
        onView(withId(R.id.volumeBar)).check(matches(not(isEnabled()))) // Vérification que le volume soit bien désactivé par défaut
        onView(withId(R.id.enableCustomVolume)).perform(click())
        onView(withId(R.id.volumeBar)).check(matches(isEnabled())) // Vérification du volume activé après avoir toggle le bouton

        // Sélection de la playlist
        onView(withId(R.id.playlists)).check(matches(isDisplayed()))
        onView(withId(R.id.playlists)).perform(
            RecyclerViewActions.actionOnItemAtPosition<PlaylistListAdapter.Companion.PlaylistViewHolder>(0, click())
        )
        onView(childAt(withId(R.id.playlists), 0)).check(matches(withBackgroundColor(R.color.colorAccent)))
        onView(withId(R.id.playlists)).perform(
            RecyclerViewActions.actionOnItemAtPosition<PlaylistListAdapter.Companion.PlaylistViewHolder>(1, click())
        )
        onView(childAt(withId(R.id.playlists), 1)).check(matches(withBackgroundColor(R.color.colorAccent)))
        onView(childAt(withId(R.id.playlists), 0)).check(matches(not(withBackgroundColor(R.color.colorAccent))))

        // Validation de la playlist into création
        onView(withId(R.id.validate)).perform(click())
        onView(withId(R.id.createParent)).check(doesNotExist()) // Fermeture de l'écran de création (matches(not(isDisplayed()) ne fonctionne pas)
        onView(withId(R.id.alarmListParent)).check(matches(isDisplayed())) // Retour sur l'écran de liste
        // Vérification que l'alarme créée existe bien
        onView(withText(titleText)).check(matches(isDisplayed()))
    }
}