package com.chazalon.dorian.aube.domain.repository

import com.chazalon.dorian.aube.domain.model.MusicState
import kotlinx.coroutines.channels.SendChannel


interface PlayMusicRepository {
    suspend fun connect(silent: Boolean)
    suspend fun disconnect()
    suspend fun play(playlistId: String)
    suspend fun skip()
    suspend fun pause()

    fun registerMusicEvents(channel: SendChannel<MusicState>)
}