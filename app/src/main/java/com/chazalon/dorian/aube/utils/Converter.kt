package com.chazalon.dorian.aube.utils


interface Converter<E, M> {
    fun convert(obj: E): M

    fun convert(collection: Collection<E>): List<M> =
        ArrayList(collection.map { convert(it) })
}