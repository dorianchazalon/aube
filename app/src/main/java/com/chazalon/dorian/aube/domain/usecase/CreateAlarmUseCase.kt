package com.chazalon.dorian.aube.domain.usecase

import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.domain.repository.AlarmRepository


class CreateAlarmUseCase(private val alarmRepository: AlarmRepository) {

    suspend fun createAlarm(
        title: String, days: List<Day>, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?, active: Boolean = true
    ): Alarm {
        return alarmRepository.insertOrReplaceAlarm(
            title,
            days,
            hour,
            minute,
            useCustomVolume,
            volume,
            playlistId,
            active
        )
    }
}