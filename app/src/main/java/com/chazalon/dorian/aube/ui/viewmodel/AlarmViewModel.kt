package com.chazalon.dorian.aube.ui.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.usecase.GetAlarmUseCase
import com.chazalon.dorian.aube.ui.extension.buildDetail
import com.chazalon.dorian.aube.ui.extension.buildList
import com.chazalon.dorian.aube.ui.extension.selectDetailFromList
import com.chazalon.dorian.aube.ui.view.converter.AlarmListViewConverter
import com.chazalon.dorian.aube.ui.view.converter.EditAlarmViewConverter
import com.chazalon.dorian.aube.ui.viewmodel.base.AubeViewModel


class AlarmViewModel(
    application: Application,
    private val getAlarmUseCase: GetAlarmUseCase
) : AubeViewModel<AlarmViewModel.AlarmCallbackView>(application) {

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AlarmViewModel::class.java)) {
                val getAlarmListUseCase = GetAlarmUseCase(AlarmDataRepository.getInstance())

                return AlarmViewModel(application, getAlarmListUseCase) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    private val _alarmList = MutableLiveData<List<Alarm>>()
    private val _alarmDetail = MutableLiveData<Alarm>()

    val alarmList = _alarmList.buildList(AlarmListViewConverter(application))
    val alarmDetail = _alarmDetail.buildDetail(EditAlarmViewConverter())

    fun getAlarmList() {
        execute {
            callView { it.onAlarmListLoad(true) }
            _alarmList.value = getAlarmUseCase.getAllAlarms()
            callView { it.onAlarmListLoad(false) }
        }
    }

    fun getAlarm(id: Int) {
        execute(
            onError = {
                callView { it.onAlarmError() }
            }
        ) {
            callView { it.onAlarmLoad(true) }
            _alarmDetail.value = getAlarmUseCase.getAlarm(id)
            callView { it.onAlarmLoad(false) }
        }
    }

    fun selectAlarm(alarmId: Int) {
        _alarmDetail.selectDetailFromList(_alarmList, alarmId) { model, id -> model.id == id }
    }

    interface AlarmCallbackView {
        fun onAlarmListLoad(loading: Boolean) {}

        fun onAlarmLoad(loading: Boolean) {}
        fun onAlarmError() {}
    }
}