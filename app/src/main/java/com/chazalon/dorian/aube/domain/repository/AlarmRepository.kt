package com.chazalon.dorian.aube.domain.repository

import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.Day


interface AlarmRepository {
    suspend fun allAlarms(): List<Alarm>
    suspend fun alarm(id: Int): Alarm

    suspend fun insertOrReplaceAlarm(
        title: String, days: List<Day>, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?, active: Boolean
    ): Alarm

    suspend fun updateAlarm(
        id: Int, title: String, days: List<Day>, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?
    )

    suspend fun updateAlarmActive(id: Int, active: Boolean)
    suspend fun updateAlarmLastTriggerTime(id: Int, lastTriggerTime: Long)

    suspend fun snoozeAlarm(id: Int, delay: Int)
    suspend fun createNextDayAlarm(id: Int)

    suspend fun delete(id: Int)
}