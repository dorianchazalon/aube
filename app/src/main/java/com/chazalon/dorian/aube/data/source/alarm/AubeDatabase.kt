package com.chazalon.dorian.aube.data.source.alarm

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.chazalon.dorian.aube.data.model.AlarmEntity
import com.chazalon.dorian.aube.data.source.alarm.dao.AlarmDao


private const val DATABASE_NAME: String = "aube.db"

@Database(
    version = 1,
    exportSchema = false,
    entities = [AlarmEntity::class]
)
abstract class AubeDatabase : RoomDatabase() {

    companion object {
        private var INSTANCE: AubeDatabase? = null

        fun init(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext, AubeDatabase::class.java,
                    DATABASE_NAME
                )
                    .build()
            }
        }

        fun db(): AubeDatabase {
            return INSTANCE!!
        }
    }

    abstract fun alarmDao(): AlarmDao
}