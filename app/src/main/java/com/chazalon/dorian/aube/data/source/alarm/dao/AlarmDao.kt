package com.chazalon.dorian.aube.data.source.alarm.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chazalon.dorian.aube.data.model.AlarmEntity


@Dao
interface AlarmDao {

    @Query("SELECT * FROM AlarmEntity")
    suspend fun allAlarms(): List<AlarmEntity>

    @Query("SELECT * FROM AlarmEntity WHERE id = :id")
    suspend fun alarm(id: Int): AlarmEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplaceAlarm(alarm: AlarmEntity): Long

    @Query("UPDATE AlarmEntity SET title = :title, days = :days, hour = :hour, minute = :minute, useCustomVolume = :useCustomVolume, volume = :volume, playlistId = :playlistId WHERE id = :id")
    suspend fun updateAlarm(
        id: Int, title: String, days: Int, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?
    )

    @Query("UPDATE AlarmEntity SET active = :active WHERE id = :id")
    suspend fun updateAlarmActive(id: Int, active: Boolean)

    @Query("UPDATE AlarmEntity SET lastTriggerTime = :lastTriggerTime WHERE id = :id")
    suspend fun updateAlarmLastTriggerTime(id: Int, lastTriggerTime: Long?)

    @Query("DELETE FROM AlarmEntity WHERE id = :id")
    suspend fun deleteAlarm(id: Int)
}