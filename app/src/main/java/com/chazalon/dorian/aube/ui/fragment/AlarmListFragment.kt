package com.chazalon.dorian.aube.ui.fragment


import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.databinding.FragmentAlarmListBinding
import com.chazalon.dorian.aube.ui.adapter.AlarmListAdapter
import com.chazalon.dorian.aube.ui.extension.gone
import com.chazalon.dorian.aube.ui.extension.visible
import com.chazalon.dorian.aube.ui.view.AlarmListItemView
import com.chazalon.dorian.aube.ui.viewmodel.AlarmListViewModel
import com.chazalon.dorian.aube.ui.viewmodel.AlarmViewModel
import com.chazalon.dorian.aube.utils.Hack
import com.chazalon.dorian.aube.utils.SettingsUtils

private const val HACK_COUNT = 5

class AlarmListFragment : Fragment(), AlarmViewModel.AlarmCallbackView {

    private val alarmViewModel by lazy {
        ViewModelProvider(requireActivity(), AlarmViewModel.Factory(requireActivity().application))
            .get(AlarmViewModel::class.java)
    }

    private val viewModel by lazy {
        ViewModelProvider(this, AlarmListViewModel.Factory(requireActivity().application))
            .get(AlarmListViewModel::class.java)
    }

    private val adapter = AlarmListAdapter()

    private lateinit var binding: FragmentAlarmListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAlarmListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        alarmViewModel.addCallbackView(viewLifecycleOwner, this)
        alarmViewModel.alarmList.observe(viewLifecycleOwner) { renderAlarmList(it) }

        if (savedInstanceState == null) {
            alarmViewModel.getAlarmList()
        }
    }

    private fun initViews() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)

        adapter.clickListener = object : AlarmListAdapter.OnItemClickListener {
            override fun onAlarmClicked(id: Int) {
                alarmViewModel.selectAlarm(id)
                findNavController().navigate(R.id.action_alarmListFragment_to_editAlarmFragment)
            }

            override fun onAlarmLongClicked(lastTrigger: String): Boolean {
                Toast.makeText(requireContext(), lastTrigger, Toast.LENGTH_LONG).show()
                return true
            }

            override fun onAlarmActiveChanged(itemView: AlarmListItemView) {
                viewModel.editAlarmActive(itemView)
            }
        }

        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter

        binding.addAlarm.setOnClickListener { findNavController().navigate(R.id.action_alarmListFragment_to_createAlarmFragment) }

        initHack()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.menu_settings -> {
                findNavController().navigate(R.id.action_alarmListFragment_to_settingsFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onAlarmListLoad(loading: Boolean) {
        if (loading) binding.progressBar.visible() else binding.progressBar.gone()
    }

    private fun renderAlarmList(data: List<AlarmListItemView>) {
        adapter.itemViews = data
    }

    // En gros faut taper $HACK_COUNT fois la toolbar avec moins de 500ms d'intervalle entre chaque touche pour lancer la création auto des alarmes
    // Ne marche qu'une seule fois par installation
    private val hackHandler = Handler(Looper.getMainLooper())
    private var hackCount = HACK_COUNT
    private var hackTriggered = false
    private fun initHack() {
        hackTriggered = SettingsUtils.isHackDone(requireContext())

        binding.toolbar.setOnClickListener {
            if (!hackTriggered) {
                hackHandler.removeCallbacksAndMessages(null)
                hackCount--
                if (hackCount <= 0) {
                    hackTriggered = true
                    SettingsUtils.hackDone(requireContext())
                    Hack.hack(object : Hack.OnHackListener {
                        override fun success() {
                            alarmViewModel.getAlarmList()
                        }
                    })
                } else {
                    hackHandler.postDelayed({
                        hackCount = HACK_COUNT
                    }, 500)
                }
            }
        }
    }
}
