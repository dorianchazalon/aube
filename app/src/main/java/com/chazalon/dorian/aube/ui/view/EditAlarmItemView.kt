package com.chazalon.dorian.aube.ui.view


data class EditAlarmItemView(
    val id: Int,
    val title: String,
    val days: WeekItemView,
    val hour: Int,
    val minute: Int,
    val useCustomVolume: Boolean,
    val volume: Int,
    val playlistId: String?
)