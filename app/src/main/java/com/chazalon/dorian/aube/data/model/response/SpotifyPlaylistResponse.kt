package com.chazalon.dorian.aube.data.model.response

import com.chazalon.dorian.aube.data.model.PlaylistEntity
import com.google.gson.annotations.SerializedName


data class SpotifyPlaylistResponse(
    @SerializedName("items") val playlists: List<PlaylistEntity>
)