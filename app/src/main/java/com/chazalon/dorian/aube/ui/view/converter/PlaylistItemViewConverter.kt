package com.chazalon.dorian.aube.ui.view.converter

import com.chazalon.dorian.aube.domain.model.Playlist
import com.chazalon.dorian.aube.ui.view.PlaylistItemView
import com.chazalon.dorian.aube.utils.Converter


class PlaylistItemViewConverter : Converter<Playlist, PlaylistItemView> {
    override fun convert(obj: Playlist): PlaylistItemView {
        return PlaylistItemView(obj.id, obj.title, obj.total, obj.image)
    }
}