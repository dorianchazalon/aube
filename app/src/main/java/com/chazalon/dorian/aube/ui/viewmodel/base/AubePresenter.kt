package com.chazalon.dorian.aube.ui.viewmodel.base

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce


abstract class AubePresenter<View> {

    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    private val callbackView = mutableSetOf<View>()

    protected fun execute(
        onError: suspend (e: Exception) -> Unit = {},
        request: suspend () -> Unit
    ): Job =
        scope.launch {
            try {
                request()
            } catch (e: Exception) {
                onError(e)
            }
        }

    fun addCallbackView(view: View): Boolean =
        callbackView.add(view)

    fun detachCallbackView(view: View): Boolean =
        callbackView.remove(view)

    protected suspend fun callView(action: (View) -> Unit) {
        withContext(Dispatchers.Main) {
            produce {
                for (cv in callbackView) {
                    send(cv)
                }
            }.consumeEach(action)
        }
    }
}