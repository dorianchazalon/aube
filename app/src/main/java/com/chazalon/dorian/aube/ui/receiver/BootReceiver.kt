package com.chazalon.dorian.aube.ui.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.domain.usecase.EditAlarmUseCase
import com.chazalon.dorian.aube.domain.usecase.GetAlarmUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            Log.d("BootReceiver", "boot completed received")

            val getAlarmListUseCase = GetAlarmUseCase(AlarmDataRepository.getInstance())
            val editAlarmUseCase = EditAlarmUseCase(AlarmDataRepository.getInstance())

            GlobalScope.launch(Dispatchers.IO) {
                val alarms = getAlarmListUseCase.getAllAlarms()
                editAlarmUseCase.restartAllAlarms(
                    alarms
                        .filter { it.active }
                        .map { it.id }
                )
            }
        }
    }
}
