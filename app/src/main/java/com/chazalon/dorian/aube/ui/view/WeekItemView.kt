package com.chazalon.dorian.aube.ui.view


data class WeekItemView(
    val days: List<DayItemView>
)