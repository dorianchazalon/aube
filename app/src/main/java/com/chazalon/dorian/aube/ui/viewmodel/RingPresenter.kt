package com.chazalon.dorian.aube.ui.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.data.repository.PlaySpotifyRepository
import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.MusicState
import com.chazalon.dorian.aube.domain.usecase.EditAlarmUseCase
import com.chazalon.dorian.aube.domain.usecase.GetAlarmUseCase
import com.chazalon.dorian.aube.domain.usecase.PlayMusicUseCase
import com.chazalon.dorian.aube.ui.extension.buildDetail
import com.chazalon.dorian.aube.ui.view.converter.MusicStateViewConverter
import com.chazalon.dorian.aube.ui.view.converter.RingAlarmViewConverter
import com.chazalon.dorian.aube.ui.viewmodel.base.AubePresenter
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import java.util.*


class RingPresenter(
    application: Application,
    private val getAlarmUseCase: GetAlarmUseCase,
    private val editAlarmUseCase: EditAlarmUseCase,
    private val playMusicUseCase: PlayMusicUseCase
) : AubePresenter<RingPresenter.RingCallbackView>() {

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) {

        fun create(): RingPresenter {

            val getAlarmUseCase = GetAlarmUseCase(AlarmDataRepository.getInstance())
            val editAlarmUseCase = EditAlarmUseCase(AlarmDataRepository.getInstance())

            val playMusicUseCase = PlayMusicUseCase(PlaySpotifyRepository.getInstance())

            return RingPresenter(
                application,
                getAlarmUseCase,
                editAlarmUseCase,
                playMusicUseCase
            )
        }
    }

    private val _alarm = MutableLiveData<Alarm>()
    private val _musicState = MutableLiveData<MusicState>()

    val alarm = _alarm.buildDetail(RingAlarmViewConverter())
    val musicState = _musicState.buildDetail(MusicStateViewConverter(application))

    fun connect() {
        execute(
            onError = { callView { it.onConnect(false) } }
        )
        {
            playMusicUseCase.connect(true)
            registerMusicEvents()
            callView { it.onConnect(true) }
        }
    }

    fun disconnect() {
        execute { playMusicUseCase.disconnect() }
    }


    private fun registerMusicEvents() {
        execute {
            Channel<MusicState>().let { channel ->
                playMusicUseCase.musicEvents(channel)
                channel.consumeEach { musicState -> _musicState.value = musicState }
            }
        }
    }

    fun playMusic(playlistId: String) {
        execute(
            onError = { callView { it.onPlay(false) } }
        )
        {
            playMusicUseCase.playMusic(playlistId)
            callView { it.onPlay(true) }
        }
    }

    fun skipMusic() {
        execute(
            onError = { callView { it.onSkip(false) } }
        ) {
            playMusicUseCase.skipMusic()
            callView { it.onSkip(true) }
        }
    }

    fun snooze(id: Int?, delay: Int) {
        execute {
            id?.let { editAlarmUseCase.snoozeAlarm(it, delay) }
            playMusicUseCase.pauseMusic()
            callView { it.onSnoozed() }
        }
    }

    fun dismiss() {
        execute {
            playMusicUseCase.pauseMusic()
            callView { it.onDismissed() }
        }
    }

    fun getAlarm(id: Int) {
        execute(
            onError = { callView { it.onAlarmError() } }
        ) {
            val alarmModel = getAlarmUseCase.getAlarm(id)

            val trigger = Calendar.getInstance().timeInMillis
            val newAlarm = alarmModel.copy(lastTriggerTime = trigger)
            editAlarmUseCase.createNextDayAlarm(id)
            _alarm.value = newAlarm
            editAlarmUseCase.editLastTriggerTime(alarmModel.id, trigger)
        }
    }

    interface RingCallbackView {
        // Alarm
        fun onAlarmError()

        // Music event
        fun onConnect(success: Boolean)
        fun onPlay(success: Boolean)
        fun onSkip(success: Boolean)
        fun onSnoozed()
        fun onDismissed()
    }
}