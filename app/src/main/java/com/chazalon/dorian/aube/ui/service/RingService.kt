package com.chazalon.dorian.aube.ui.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.*
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Observer
import com.chazalon.dorian.aube.ui.notification.AubeNotificationManager
import com.chazalon.dorian.aube.ui.view.MusicStateItemView
import com.chazalon.dorian.aube.ui.view.RingAlarmItemView
import com.chazalon.dorian.aube.ui.viewmodel.RingPresenter
import com.chazalon.dorian.aube.utils.AudioUtils
import com.chazalon.dorian.aube.utils.SettingsUtils
import java.util.*

const val RING_SERVICE_ID = 1430


private const val ACTION_START = "ACTION_START"
private const val ACTION_SNOOZE = "ACTION_SNOOZE"
private const val ACTION_DISMISS = "ACTION_DISMISS"

private const val EXTRA_ALARM_ID = "EXTRA_ALARM_ID"

private const val VIBRATE_DURATION = 1000L // en millis


class RingService : Service(), RingPresenter.RingCallbackView {

    private val presenter by lazy {
        RingPresenter.Factory(application).create()
    }

    private val binder = RingBinder()

    private var ringItemView: RingAlarmItemView? = null

    private lateinit var ringtone: Ringtone

    var ringCallback: RingCallback? = null
    var currentState: MusicStateItemView? = null

    private lateinit var timer: Timer
    private var connected = false
    private var connectRetryLeft = 5

    override fun onCreate() {
        super.onCreate()

        initRingtone()
        initTimer()

        presenter.addCallbackView(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachCallbackView(this)
        presenter.disconnect()
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        when (intent?.action) {
            ACTION_START -> {
                start(intent)
            }
            ACTION_SNOOZE -> {
                snooze()
            }
            ACTION_DISMISS -> {
                dismiss()
            }
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder = binder

    private fun initRingtone() {
        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        ringtone = RingtoneManager.getRingtone(this, uri)
        val audioAttributes = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_ALARM)
            .setLegacyStreamType(AudioManager.STREAM_ALARM)
            .build()
        ringtone.audioAttributes = audioAttributes
    }

    private fun initVolume() {
        if (ringItemView?.useCustomVolume == true) {
            AudioUtils.setVolume(this, ringItemView?.volume ?: AudioUtils.ERROR_VOLUME)
        } else {
            if (SettingsUtils.useDefaultVolume(this)) {
                AudioUtils.setVolume(this, SettingsUtils.getDefaultVolume(this))
            }
        }
    }

    private fun initTimer() {
        timer = Timer()
        timer.scheduleAtFixedRate(
            object : TimerTask() {
                override fun run() {
                    if (AudioUtils.isNotificationSoundActivated(this@RingService)) {
                        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                        val ringt = RingtoneManager.getRingtone(this@RingService, uri)
                        val audioAttributes = AudioAttributes.Builder()
                            .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                            .build()
                        ringt.audioAttributes = audioAttributes
                        ringt.play()
                    } else {
                        val vibrator = this@RingService.getSystemService(Vibrator::class.java)

                        val effect = VibrationEffect.createOneShot(VIBRATE_DURATION, 200)
                        vibrator!!.vibrate(effect)
                    }
                }
            }, (SettingsUtils.getSnoozeDelay(this) * 60 * 1000).toLong(),
            (SettingsUtils.getSnoozeDelay(this) * 60 * 1000).toLong()
        )
    }


    private fun start(intent: Intent?) {

        presenter.alarm.observeForever(alarmObserver)
        presenter.musicState.observeForever(musicStateObserver)

        val id = intent?.extras?.getInt(EXTRA_ALARM_ID)

        // Quand on lance un service en foreground, il faut appeler la méthode startForeground pour notifier l'utilisateur rapidement
        // Mais on nous autorise quand même un petit délai avant ça pour des opérations nécessaires, comme ici aller récupérer les infos de l'alarme
        if (id != null) {
            presenter.getAlarm(id)
        } else {
            throw MissingFormatArgumentException("$EXTRA_ALARM_ID is missing")
        }
    }

    private fun playMusic() =
        ringItemView?.playlistId?.let { presenter.playMusic(it) } ?: playBasicRingtone()


    private fun retryMusicConnection() {
        if (connectRetryLeft > 0) {
            Handler(Looper.getMainLooper()).postDelayed({
                presenter.connect()
                connectRetryLeft--
            }, 2000)
        }
    }

    private fun playBasicRingtone() {
        if (!ringtone.isPlaying) {
            ringtone.play()
        }
    }

    private fun stopBasicRingtone() {
        ringtone.stop()
    }


    fun snooze() {
        stopBasicRingtone()
        resetVolume()
        reset()
        presenter.snooze(ringItemView?.id, SettingsUtils.getSnoozeDelay(this))
    }


    fun dismiss() {
        stopBasicRingtone()
        resetVolume()
        reset()
        presenter.dismiss()
    }

    fun skip() {
        presenter.skipMusic()
    }


    private fun reset() {
        with(presenter) {
            alarm.removeObserver(alarmObserver)

            musicState.removeObserver(musicStateObserver)
        }

        timer.cancel()
    }

    private fun resetVolume() {
        if (SettingsUtils.turnOffAutomatically(this)) {
            AudioUtils.setVolume(this, 0)
        }
    }

    private val alarmObserver = Observer<RingAlarmItemView> { data ->
        ringItemView = data
        // Une fois qu'on a récupéré les infos de l'alarme, on doit lancer le service avec une notif pour l'utilisateur obligatoirement
        // C'est le système qui déterminera s'il faut afficher juste la notification ou notre plein écran (RingActivity) (le plus souvent écran allumé = notification uniquement)
        // Voir setFullScreenIntent() dans la notification
        startForeground(
            RING_SERVICE_ID,
            AubeNotificationManager.getInstance().buildRingNotification(this, data)
        )
        initVolume()
        ringItemView?.playlistId?.run {
            presenter.connect()
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    if (!connected) {
                        playBasicRingtone()
                    }
                },
                3000
            ) // Si la connexion prend du temps et n'a pas été établie au bout de 3 sec, on lance le ringtone par précaution (en attendant les retry)
        } ?: playBasicRingtone()
    }

    private val musicStateObserver = Observer<MusicStateItemView> { state ->
        if (ringCallback != null) {
            ringItemView?.let {
                NotificationManagerCompat.from(this).notify(
                    RING_SERVICE_ID,
                    AubeNotificationManager.getInstance()
                        .updateRingNotification(this, it, state.track)
                )
            }
        } else {
            ringItemView?.let {
                NotificationManagerCompat.from(this).notify(
                    RING_SERVICE_ID,
                    AubeNotificationManager.getInstance()
                        .buildRingNotification(this, it, state.track)
                )
            }
        }

        currentState = state
        ringCallback?.onStateChanged(state)
    }

    inner class RingBinder : Binder() {
        fun getService(): RingService = this@RingService
    }

    interface RingCallback {
        fun onStateChanged(state: MusicStateItemView)
        fun onSnoozed()
        fun onDismissed()
    }

    companion object {
        fun buildStartIntent(context: Context, alarmId: Int): Intent =
            Intent(context, RingService::class.java).apply {
                putExtra(EXTRA_ALARM_ID, alarmId)
                action = ACTION_START
            }

        fun buildSnoozeIntent(context: Context, alarm: RingAlarmItemView): Intent =
            Intent(context, RingService::class.java).apply {
                putExtra(EXTRA_ALARM_ID, alarm.id)
                action = ACTION_SNOOZE
            }

        fun buildDismissIntent(context: Context, alarm: RingAlarmItemView): Intent =
            Intent(context, RingService::class.java).apply {
                putExtra(EXTRA_ALARM_ID, alarm.id)
                action = ACTION_DISMISS
            }
    }

    override fun onAlarmError() {
        throw MissingFormatArgumentException("$EXTRA_ALARM_ID is missing")
    }

    override fun onConnect(success: Boolean) {
        if (success) {
            connected = true
            stopBasicRingtone()
            playMusic()
        } else {
            connected = false
            playBasicRingtone()
            retryMusicConnection()
        }
    }

    override fun onPlay(success: Boolean) {
        if (success) {
            stopBasicRingtone()
        } else {
            playBasicRingtone()
        }
    }

    override fun onSkip(success: Boolean) {}

    /*
    Une fois le snooze ou le dismiss bien déroulé, on peut arrêter le service en foreground et notifier les callbacks (l'interface)
    C'est important pour bien laisser le temps aux processus asynchrones (comme arrêter la musique, créer la prochaine alerte) de
    s'exécuter avant de fermer le service grâce auxquels ils tournent
    Mais ça permet aussi de donner les dernières instructions à l'interface au cas où on utilise les actions de la notification directement
     */

    override fun onSnoozed() {
        ringCallback?.onSnoozed()
        ringCallback = null
        presenter.detachCallbackView(this)
        presenter.disconnect()
        stopForeground(true)
    }

    override fun onDismissed() {
        ringCallback?.onDismissed()
        ringCallback = null
        presenter.detachCallbackView(this)
        presenter.disconnect()
        stopForeground(true)
    }
}