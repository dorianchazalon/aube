package com.chazalon.dorian.aube.domain.usecase

import com.chazalon.dorian.aube.domain.model.Playlist
import com.chazalon.dorian.aube.domain.repository.InfoMusicRepository


class InfoMusicUseCase(private val infoMusicRepository: InfoMusicRepository) {

    suspend fun connect() {
        infoMusicRepository.connect()
    }

    suspend fun playlists(): List<Playlist> {
        return infoMusicRepository.playlists()
    }
}