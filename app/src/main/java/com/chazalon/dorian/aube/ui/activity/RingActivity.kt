package com.chazalon.dorian.aube.ui.activity

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.chazalon.dorian.aube.R

class RingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.addFlags(
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    or WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_ring)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.fragment_host).navigateUp()

    override fun onBackPressed() {
        // On empêche de fermer l'activity avec le bouton back pour forcer le snooze ou l'arrêt de l'alarme
    }
}
