package com.chazalon.dorian.aube.ui.layout

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.databinding.WeekToggleBinding
import com.chazalon.dorian.aube.ui.view.DayItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView


class WeekLayout(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    private var binding: WeekToggleBinding =
        WeekToggleBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.WeekLayout)
        val editable = a.getBoolean(R.styleable.WeekLayout_editable, true)

        a.recycle()

        with(binding) {
            mutableListOf(monday, tuesday, wednesday, thursday, friday, saturday, sunday)
                .forEach {
                    it.isEnabled = editable
                    it.isClickable = editable
                }
        }
    }

    fun setItems(itemView: WeekItemView) {
        with(binding) {
            monday.isChecked = itemView.days.contains(DayItemView.MONDAY)
            tuesday.isChecked = itemView.days.contains(DayItemView.TUESDAY)
            wednesday.isChecked = itemView.days.contains(DayItemView.WEDNESDAY)
            thursday.isChecked = itemView.days.contains(DayItemView.THURSDAY)
            friday.isChecked = itemView.days.contains(DayItemView.FRIDAY)
            saturday.isChecked = itemView.days.contains(DayItemView.SATURDAY)
            sunday.isChecked = itemView.days.contains(DayItemView.SUNDAY)
        }
    }

    fun getSelectedDays(): List<DayItemView> {
        val days = ArrayList<DayItemView>()

        with(binding) {
            if (monday.isChecked) {
                days.add(DayItemView.MONDAY)
            }
            if (tuesday.isChecked) {
                days.add(DayItemView.TUESDAY)
            }
            if (wednesday.isChecked) {
                days.add(DayItemView.WEDNESDAY)
            }
            if (thursday.isChecked) {
                days.add(DayItemView.THURSDAY)
            }
            if (friday.isChecked) {
                days.add(DayItemView.FRIDAY)
            }
            if (saturday.isChecked) {
                days.add(DayItemView.SATURDAY)
            }
            if (sunday.isChecked) {
                days.add(DayItemView.SUNDAY)
            }
        }

        return days
    }
}