package com.chazalon.dorian.aube.data.source.music

import android.text.TextUtils
import android.util.Log
import com.chazalon.dorian.aube.BuildConfig
import com.chazalon.dorian.aube.data.source.SPOTIFY_BASE_URL
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


private const val AUTHORIZATION = "Authorization"
private const val BEARER = "Bearer "

class SpotifyService {

    companion object {

        private var service: ISpotifyService? = null
        private var token: String? = null

        fun getService(token: String): ISpotifyService {
            ensureService(token)
            return service!!
        }

        private fun ensureService(token: String) {
            if (service == null || TextUtils.isEmpty(SpotifyService.token) || token != SpotifyService.token) {
                Log.d("SpotifyService", "token: $token")
                SpotifyService.token = token
                service = buildService()
            }
        }

        private fun buildService(): ISpotifyService {
            val builder = OkHttpClient.Builder()

            if (BuildConfig.DEBUG) {
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(logging)
            }

            builder.authenticator(object : Authenticator {
                override fun authenticate(route: Route?, response: Response): Request {
                    return response.request.newBuilder().header(
                        AUTHORIZATION,
                        BEARER + token!!
                    ).build()
                }
            })

            val retrofit = Retrofit.Builder()
                .baseUrl(SPOTIFY_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build()

            return retrofit.create(ISpotifyService::class.java)
        }
    }

}