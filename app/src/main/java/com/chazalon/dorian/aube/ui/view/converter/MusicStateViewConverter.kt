package com.chazalon.dorian.aube.ui.view.converter

import android.content.Context
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.domain.model.MusicState
import com.chazalon.dorian.aube.ui.view.MusicStateItemView
import com.chazalon.dorian.aube.utils.Converter


class MusicStateViewConverter(private val context: Context) :
    Converter<MusicState, MusicStateItemView> {
    override fun convert(obj: MusicState): MusicStateItemView {
        return MusicStateItemView(context.getString(R.string.music_display, obj.artist, obj.song))
    }
}