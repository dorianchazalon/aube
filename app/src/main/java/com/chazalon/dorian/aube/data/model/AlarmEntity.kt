package com.chazalon.dorian.aube.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class AlarmEntity(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    var title: String,
    var active: Boolean,
    var days: Int,
    var hour: Int,
    var minute: Int,
    var useCustomVolume: Boolean,
    var volume: Int,
    var lastTriggerTime: Long?,
    var playlistId: String?
)