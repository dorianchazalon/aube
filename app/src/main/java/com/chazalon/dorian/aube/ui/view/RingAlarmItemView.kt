package com.chazalon.dorian.aube.ui.view


data class RingAlarmItemView(
    val id: Int,
    val title: String,
    val useCustomVolume: Boolean,
    val volume: Int,
    val playlistId: String?
)