package com.chazalon.dorian.aube.data.source.music

import com.chazalon.dorian.aube.data.model.response.SpotifyPlaylistResponse
import retrofit2.http.GET


interface ISpotifyService {

    @GET("me/playlists")
    suspend fun playlists(): SpotifyPlaylistResponse
}