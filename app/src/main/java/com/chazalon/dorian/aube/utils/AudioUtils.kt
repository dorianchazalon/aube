package com.chazalon.dorian.aube.utils

import android.content.Context
import android.media.AudioManager


class AudioUtils {

    companion object {

        const val ERROR_VOLUME = 4

        fun setVolume(context: Context, volume: Int) {
            val audioManager = context.getSystemService(AudioManager::class.java)

            audioManager!!.setStreamVolume(
                AudioManager.STREAM_MUSIC, // Stream type
                volume, // Index
                0 // Flags
            )
        }

        fun getMax(context: Context): Int {
            val audioManager = context.getSystemService(AudioManager::class.java)

            return audioManager!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        }

        fun isNotificationSoundActivated(context: Context): Boolean {
            val audioManager = context.getSystemService(AudioManager::class.java)

            return audioManager!!.getStreamVolume(AudioManager.STREAM_NOTIFICATION) > 0
        }
    }

}