package com.chazalon.dorian.aube.ui.fragment


import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.databinding.FragmentEditAlarmBinding
import com.chazalon.dorian.aube.ui.adapter.PlaylistListAdapter
import com.chazalon.dorian.aube.ui.extension.enable
import com.chazalon.dorian.aube.ui.extension.gone
import com.chazalon.dorian.aube.ui.extension.visible
import com.chazalon.dorian.aube.ui.view.EditAlarmItemView
import com.chazalon.dorian.aube.ui.view.PlaylistItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.ui.viewmodel.AlarmViewModel
import com.chazalon.dorian.aube.ui.viewmodel.EditAlarmViewModel
import com.chazalon.dorian.aube.utils.AudioUtils
import com.google.android.material.snackbar.Snackbar


class EditAlarmFragment : Fragment(), AlarmViewModel.AlarmCallbackView,
    EditAlarmViewModel.EditAlarmCallbackView {

    private val alarmViewModel by lazy {
        ViewModelProvider(requireActivity(), AlarmViewModel.Factory(requireActivity().application))
            .get(AlarmViewModel::class.java)
    }

    private val viewModel by lazy {
        ViewModelProvider(this, EditAlarmViewModel.Factory(requireActivity().application))
            .get(EditAlarmViewModel::class.java)
    }

    private lateinit var binding: FragmentEditAlarmBinding

    private val playlistListAdapter = PlaylistListAdapter()

    private var currentItemView: EditAlarmItemView? = null

    private var hour: Int = 0
    private var minute: Int = 0

    private var needRequest = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditAlarmBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        needRequest = savedInstanceState == null

        initViews()

        alarmViewModel.addCallbackView(viewLifecycleOwner, this)
        alarmViewModel.alarmDetail.observe(viewLifecycleOwner) { renderAlarm(it) }

        viewModel.addCallbackView(viewLifecycleOwner, this)
        viewModel.playlists.observe(viewLifecycleOwner) { renderPlaylists(it) }

        if (needRequest) {
            // Si on arrive de la PendingIntent d'édit d'alarme du système
            arguments?.run {
                if (containsKey(getString(R.string.arg_alarm_id))) {
                    alarmViewModel.getAlarm(getInt(getString(R.string.arg_alarm_id)))
                }
            }
        }
    }

    private fun initViews() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.editParent.enable(false)

        binding.playlists.layoutManager = LinearLayoutManager(requireContext())
        binding.playlists.adapter = playlistListAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_edit, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.menu_delete -> {
                currentItemView?.let { viewModel.delete(it.id) }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun displayAlarm(alarm: EditAlarmItemView) {
        currentItemView = alarm

        hour = alarm.hour
        minute = alarm.minute

        binding.title.setText(alarm.title)
        binding.title.addTextChangedListener(CreateAlarmFragment.ClearWatcher(binding.title))

        binding.time.text = getString(R.string.hour_h, hour, minute)
        binding.time.setOnClickListener {
            TimePickerDialog(
                requireContext(), R.style.DialogTheme,
                { _, hourPicker: Int, minutePicker: Int ->
                    hour = hourPicker
                    minute = minutePicker
                    binding.time.text = getString(R.string.hour_h, hour, minute)
                },
                hour, minute, true
            ).show()
        }

        binding.weekLayout.setItems(alarm.days)

        binding.enableCustomVolume.isChecked = alarm.useCustomVolume
        binding.enableCustomVolume.setOnCheckedChangeListener { _, checked ->
            binding.volumeBar.isEnabled = checked
        }

        binding.volumeBar.max = AudioUtils.getMax(requireContext())
        binding.volumeBar.progress = alarm.volume
        binding.volumeBar.isEnabled = alarm.useCustomVolume

        binding.validate.setOnClickListener {
            if (checkFields()) {
                currentItemView!!.let { item ->
                    viewModel.update(
                        EditAlarmItemView(
                            item.id,
                            binding.title.text.toString(),
                            WeekItemView(binding.weekLayout.getSelectedDays()),
                            hour,
                            minute,
                            binding.enableCustomVolume.isChecked,
                            binding.volumeBar.progress,
                            playlistListAdapter.getSelectedPlaylist()?.id
                        )
                    )
                }
            }
        }
    }

    /**
     * @return true si les champs pour créer l'alarme sont valides, false sinon
     */
    private fun checkFields(): Boolean {
        var hasError = false

        if (binding.title.text.isNullOrBlank()) {
            hasError = true
            binding.title.error = getString(R.string.error_missing_title)
        }

        if (binding.weekLayout.getSelectedDays().isEmpty()) {
            hasError = true
            Snackbar.make(
                binding.editParent,
                R.string.error_missing_at_least_one_day,
                Snackbar.LENGTH_SHORT
            ).show()
        }

        return !hasError
    }

    override fun onAlarmLoad(loading: Boolean) {
        if (loading) {
            binding.progressBar.visible()
            binding.validate.gone()
        } else {
            binding.progressBar.gone()
            binding.validate.visible()
        }
    }

    override fun onAlarmError() {
        findNavController().popBackStack()
    }

    private fun renderPlaylists(data: List<PlaylistItemView>) {
        for (item in data) {
            if (currentItemView?.playlistId == item.id) {
                item.selected = true
            }
        }
        playlistListAdapter.itemViews = data
    }

    private fun renderAlarm(data: EditAlarmItemView) {
        displayAlarm(data)
        if (needRequest) {
            viewModel.connect()
        } else {
            enableUI()
        }
    }

    private fun enableUI() {
        binding.editParent.enable(true)
        binding.volumeBar.isEnabled = binding.enableCustomVolume.isChecked
    }

    class ClearWatcher(private val editText: EditText) : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            editText.error = null
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
    }

    override fun onMusicConnected() {
        enableUI()

        viewModel.playlists()
    }

    override fun onMusicConnectionError() {
        enableUI()

        Snackbar.make(binding.editParent, R.string.error_music_connection, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun onPlaylistLoad(loading: Boolean) {
        if (!loading) {
            if (playlistListAdapter.itemCount > 0) {
                binding.playlists.visible()
            } else {
                binding.playlists.gone()
            }
        }
    }

    override fun onPlaylistError() {
        Snackbar.make(binding.editParent, R.string.error_playlist_list, Snackbar.LENGTH_SHORT)
            .show()
    }

    override fun onEditAlarmLoad(loading: Boolean) {
        if (loading) binding.progressBar.visible() else binding.progressBar.gone()
    }

    override fun onEditAlarmSuccess() {
        findNavController().popBackStack()
    }

    override fun onEditAlarmError(message: String?) {
        Snackbar.make(binding.editParent, message ?: "Erreur", Snackbar.LENGTH_SHORT).show()
    }
}
