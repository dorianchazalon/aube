package com.chazalon.dorian.aube.ui.extension

import android.view.View
import android.view.ViewGroup
import androidx.core.view.children


fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.enable(enabled: Boolean) {
    this.isEnabled = enabled
    if (this is ViewGroup) {
        children.forEach { it.enable(enabled) }
    }
}