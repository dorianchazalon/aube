package com.chazalon.dorian.aube.data.model

import com.google.gson.annotations.SerializedName


data class PlaylistEntity(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("public") val public: Boolean,
    @SerializedName("tracks") private val tracks: TracksEntity,
    @SerializedName("images") private val images: List<ImageEntity>?
) {
    fun getTotal(): Int {
        return tracks.total
    }

    fun getImage(): String? {
        return images?.get(0)?.url
    }
}

data class TracksEntity(
    @SerializedName("total") val total: Int
)

data class ImageEntity(
    @SerializedName("url") val url: String
)