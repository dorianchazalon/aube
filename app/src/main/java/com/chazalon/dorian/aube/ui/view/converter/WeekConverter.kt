package com.chazalon.dorian.aube.ui.view.converter

import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.ui.view.DayItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.utils.Converter


class WeekConverter : Converter<WeekItemView, List<Day>> {
    override fun convert(obj: WeekItemView): List<Day> {
        val days = ArrayList<Day>()

        obj.days.forEach {
            when (it) {
                DayItemView.MONDAY -> days.add(Day.MONDAY)
                DayItemView.TUESDAY -> days.add(Day.TUESDAY)
                DayItemView.WEDNESDAY -> days.add(Day.WEDNESDAY)
                DayItemView.THURSDAY -> days.add(Day.THURSDAY)
                DayItemView.FRIDAY -> days.add(Day.FRIDAY)
                DayItemView.SATURDAY -> days.add(Day.SATURDAY)
                DayItemView.SUNDAY -> days.add(Day.SUNDAY)
            }
        }

        return days.toList()
    }
}

