package com.chazalon.dorian.aube.domain.model

enum class Day(val value: Int) {

    MONDAY(1),
    TUESDAY(1 shl 2),
    WEDNESDAY(1 shl 3),
    THURSDAY(1 shl 4),
    FRIDAY(1 shl 5),
    SATURDAY(1 shl 6),
    SUNDAY(1 shl 7);

    companion object {
        fun convertBytesToDays(bytes: Int): List<Day> = values().filter { bytes and it.value != 0 }

        fun convertDaysToBytes(days: List<Day>): Int {
            var bytes = 0
            days.map { bytes = bytes or it.value }
            return bytes
        }
    }
}