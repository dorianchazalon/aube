package com.chazalon.dorian.aube.ui.notification

import android.app.*
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavDeepLinkBuilder
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.ui.activity.RingActivity
import com.chazalon.dorian.aube.ui.service.RingService
import com.chazalon.dorian.aube.ui.view.RingAlarmItemView

private const val CHANNEL_RING_ID = "channel_ring"

class AubeNotificationManager private constructor(val application: Application) {

    init {
        // Création des channels de notification à l'initialisation
        val ringChannel = NotificationChannel(
            CHANNEL_RING_ID,
            application.getString(R.string.channel_ring_name),
            NotificationManager.IMPORTANCE_HIGH
        )
        ringChannel.description = application.getString(R.string.channel_ring_description)

        val notificationManager = application.getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(ringChannel)
    }

    private fun setupCommonBuilder(
        context: Context,
        alarm: RingAlarmItemView,
        track: String?
    ): NotificationCompat.Builder {
        val snoozeIntent = RingService.buildSnoozeIntent(context, alarm)
        val dismissIntent = RingService.buildDismissIntent(context, alarm)

        return NotificationCompat.Builder(context, CHANNEL_RING_ID)
            .setContentTitle(alarm.title)
            .setContentText(track)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setColor(ContextCompat.getColor(context, R.color.colorAccent))
            .setOngoing(true)
            .setDefaults(NotificationCompat.DEFAULT_LIGHTS)
            .setWhen(0)
            .setAutoCancel(false)
            .setCategory(NotificationCompat.CATEGORY_ALARM)
            .setLocalOnly(true)
            .addAction(
                R.drawable.ic_snooze, context.getString(R.string.alarm_alert_action_snooze),
                PendingIntent.getService(context, 0, snoozeIntent, 0)
            )
            .addAction(
                R.drawable.ic_dismiss, context.getString(R.string.alarm_alert_action_dismiss),
                PendingIntent.getService(context, 0, dismissIntent, 0)
            )
    }

    fun buildRingNotification(
        context: Context,
        alarm: RingAlarmItemView,
        track: String? = null
    ): Notification {

        val builder = setupCommonBuilder(context, alarm, track)

        val ringIntent = NavDeepLinkBuilder(context)
            .setComponentName(RingActivity::class.java)
            .setGraph(R.navigation.navigation_ring)
            .setDestination(R.id.ringFragment)
            .createPendingIntent()

        return builder
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setFullScreenIntent(ringIntent, true)
            .setContentIntent(ringIntent)
            .build()
    }

    fun updateRingNotification(
        context: Context,
        alarm: RingAlarmItemView,
        track: String? = null
    ): Notification {

        val builder = setupCommonBuilder(context, alarm, track)

        return builder
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setVisibility(NotificationCompat.VISIBILITY_SECRET)
            .build()
    }

    companion object {
        private var instance: AubeNotificationManager? = null

        fun init(application: Application) {
            if (instance == null) {
                instance = AubeNotificationManager(application)
            }
        }

        fun getInstance(): AubeNotificationManager {
            return instance!!
        }
    }
}