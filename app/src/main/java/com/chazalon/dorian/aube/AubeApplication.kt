package com.chazalon.dorian.aube

import android.app.Application
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.data.repository.InfoSpotifyRepository
import com.chazalon.dorian.aube.data.repository.PlaySpotifyRepository
import com.chazalon.dorian.aube.data.source.alarm.AubeDatabase
import com.chazalon.dorian.aube.ui.notification.AubeNotificationManager


class AubeApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        AubeDatabase.init(this)

        AubeNotificationManager.init(this)

        AlarmDataRepository.init(this)
        InfoSpotifyRepository.init(this)
        PlaySpotifyRepository.init(this)
    }
}