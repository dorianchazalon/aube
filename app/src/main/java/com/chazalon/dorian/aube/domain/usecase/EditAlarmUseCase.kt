package com.chazalon.dorian.aube.domain.usecase

import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.domain.repository.AlarmRepository


class EditAlarmUseCase(private val alarmRepository: AlarmRepository) {

    suspend fun editAlarm(
        id: Int, title: String, days: List<Day>, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?
    ) {
        alarmRepository.updateAlarm(
            id,
            title,
            days,
            hour,
            minute,
            useCustomVolume,
            volume,
            playlistId
        )
    }

    suspend fun editActive(id: Int, active: Boolean) {
        alarmRepository.updateAlarmActive(id, active)
    }

    suspend fun editLastTriggerTime(id: Int, lastTriggerTime: Long) {
        alarmRepository.updateAlarmLastTriggerTime(id, lastTriggerTime)
    }

    suspend fun deleteAlarm(id: Int) {
        alarmRepository.delete(id)
    }

    suspend fun createNextDayAlarm(id: Int) {
        alarmRepository.createNextDayAlarm(id)
    }

    suspend fun snoozeAlarm(id: Int, delay: Int) {
        alarmRepository.snoozeAlarm(id, delay)
    }

    suspend fun restartAllAlarms(ids: List<Int>) {
        ids.forEach { id ->
            editActive(id, true)
        }
    }
}