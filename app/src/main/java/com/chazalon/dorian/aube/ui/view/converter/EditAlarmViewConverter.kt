package com.chazalon.dorian.aube.ui.view.converter

import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.ui.view.EditAlarmItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.utils.Converter


class EditAlarmViewConverter : Converter<Alarm, EditAlarmItemView> {

    private val weekConverter: Converter<List<Day>, WeekItemView> = WeekViewConverter()

    override fun convert(obj: Alarm): EditAlarmItemView {
        return EditAlarmItemView(
            obj.id,
            obj.title,
            weekConverter.convert(obj.days),
            obj.hour,
            obj.minute,
            obj.useCustomVolume,
            obj.volume,
            obj.playlistId
        )
    }
}