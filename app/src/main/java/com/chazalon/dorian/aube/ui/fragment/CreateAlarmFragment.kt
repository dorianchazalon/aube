package com.chazalon.dorian.aube.ui.fragment

import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.databinding.FragmentCreateAlarmBinding
import com.chazalon.dorian.aube.ui.adapter.PlaylistListAdapter
import com.chazalon.dorian.aube.ui.extension.enable
import com.chazalon.dorian.aube.ui.extension.gone
import com.chazalon.dorian.aube.ui.extension.visible
import com.chazalon.dorian.aube.ui.view.CreateAlarmItemView
import com.chazalon.dorian.aube.ui.view.PlaylistItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.ui.viewmodel.CreateAlarmViewModel
import com.chazalon.dorian.aube.utils.AudioUtils
import com.google.android.material.snackbar.Snackbar
import java.util.*


class CreateAlarmFragment : Fragment(), CreateAlarmViewModel.CreateAlarmCallbackView {

    private val viewModel by lazy {
        ViewModelProvider(this, CreateAlarmViewModel.Factory(requireActivity().application))
            .get(CreateAlarmViewModel::class.java)
    }

    private var hour: Int
    private var minute: Int

    private lateinit var binding: FragmentCreateAlarmBinding

    private val playlistListAdapter = PlaylistListAdapter()


    init {
        val now = Calendar.getInstance()
        hour = now.get(Calendar.HOUR_OF_DAY)
        minute = now.get(Calendar.MINUTE)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateAlarmBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        viewModel.addCallbackView(viewLifecycleOwner, this)
        viewModel.playlists.observe(viewLifecycleOwner) { renderPlaylists(it) }

        if (savedInstanceState == null) {
            viewModel.connect()
        } else {
            enableUI()
        }
    }

    private fun initViews() {
        binding.createParent.enable(false)

        binding.title.addTextChangedListener(EditAlarmFragment.ClearWatcher(binding.title))

        binding.time.text = getString(R.string.hour_h, hour, minute)
        binding.time.setOnClickListener {
            TimePickerDialog(
                requireContext(), R.style.DialogTheme,
                { _, hourPicker: Int, minutePicker: Int ->
                    hour = hourPicker
                    minute = minutePicker
                    binding.time.text = getString(R.string.hour_h, hour, minute)
                },
                hour, minute, true
            ).show()
        }

        binding.enableCustomVolume.setOnCheckedChangeListener { _, checked ->
            binding.volumeBar.isEnabled = checked
        }

        binding.volumeBar.max = AudioUtils.getMax(requireContext())

        binding.playlists.layoutManager = LinearLayoutManager(requireContext())
        binding.playlists.adapter = playlistListAdapter

        binding.validate.setOnClickListener {
            if (checkFields()) {
                viewModel.createAlarm(
                    CreateAlarmItemView(
                        binding.title.text.toString(),
                        WeekItemView(binding.weekLayout.getSelectedDays()),
                        hour, minute,
                        binding.enableCustomVolume.isChecked,
                        binding.volumeBar.progress,
                        playlistListAdapter.getSelectedPlaylist()?.id
                    )
                )
            }
        }
    }

    /**
     * @return true si les champs pour créer l'alarme sont valides, false sinon
     */
    private fun checkFields(): Boolean {
        var hasError = false

        if (binding.title.text.isNullOrBlank()) {
            hasError = true
            binding.title.error = getString(R.string.error_missing_title)
        }

        if (binding.weekLayout.getSelectedDays().isEmpty()) {
            hasError = true
            Snackbar.make(
                binding.createParent,
                R.string.error_missing_at_least_one_day,
                Snackbar.LENGTH_SHORT
            ).show()
        }

        return !hasError
    }

    private fun renderPlaylists(data: List<PlaylistItemView>) {
        playlistListAdapter.itemViews = data
    }

    private fun enableUI() {
        binding.createParent.enable(true)
        binding.volumeBar.isEnabled = binding.enableCustomVolume.isChecked
    }

    class ClearWatcher(private val editText: EditText) : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            editText.error = null
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
    }

    override fun onMusicConnected() {
        enableUI()
        viewModel.playlists()
    }

    override fun onMusicConnectionError() {
        enableUI()
        Snackbar.make(binding.createParent, R.string.error_music_connection, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun onPlaylistLoad(loading: Boolean) {
        if (!loading) {
            if (playlistListAdapter.itemCount > 0) {
                binding.playlists.visible()
            } else {
                binding.playlists.gone()
            }
        }
    }

    override fun onPlaylistError() {
        Snackbar.make(binding.createParent, R.string.error_playlist_list, Snackbar.LENGTH_SHORT)
            .show()
    }

    override fun onCreateAlarmSuccess() {
        findNavController().popBackStack()
    }

    override fun onCreateAlarmError() {
        Snackbar.make(binding.createParent, R.string.error_insert_failed, Snackbar.LENGTH_SHORT)
            .show()
    }
}
