package com.chazalon.dorian.aube.data.model.converter

import com.chazalon.dorian.aube.domain.model.MusicState
import com.chazalon.dorian.aube.utils.Converter
import com.spotify.protocol.types.PlayerState


class SpotifyMusicStateConverter : Converter<PlayerState, MusicState> {
    override fun convert(obj: PlayerState): MusicState {
        return MusicState(obj.track?.artist?.name ?: "", obj.track?.name ?: "")
    }
}