package com.chazalon.dorian.aube.utils

import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.domain.usecase.CreateAlarmUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * Permet de créer mes alarmes persos rapidement dans les cas où j'ai besoin de vider la base de données pour des tests
 */
class Hack {

    companion object {

        // Playlists
        private const val INWARD = "6DWMuX3qtxysVtu0JEQUqw"
        private const val DOUBLE_FLIP = "4VsDe2OFtXtoGuekTGAYNn"

        // Jours
        private val WEEK = listOf(Day.MONDAY, Day.TUESDAY, Day.WEDNESDAY, Day.THURSDAY, Day.FRIDAY)
        private val WEEKEND = listOf(Day.SATURDAY, Day.SUNDAY)
        private val ALL_DAYS = Day.values().toList()

        fun hack(listener: OnHackListener? = null) {
            val create = CreateAlarmUseCase(AlarmDataRepository.getInstance())

            val job = Job()
            val scope = CoroutineScope(Dispatchers.Main + job)

            scope.launch {
                listener?.loading(true)

                try {
                    create.createAlarm(
                        "Matin", WEEK,
                        8, 40, true, 7, INWARD
                    )
                    create.createAlarm(
                        "Week-end", WEEKEND,
                        11, 0, true, 6, DOUBLE_FLIP
                    )
                    create.createAlarm(
                        "Fin de la pause", WEEK,
                        13, 52, true, 11, INWARD
                    )
                    create.createAlarm(
                        "Vacances", ALL_DAYS,
                        12, 0, true, 5, DOUBLE_FLIP,
                        false
                    )

                    listener?.success()
                } catch (e: Exception) {
                    listener?.error(e.message)
                }

                listener?.loading(false)
            }
        }
    }

    interface OnHackListener {
        fun loading(load: Boolean) {}
        fun success() {}
        fun error(message: String? = null) {}
    }
}