package com.chazalon.dorian.aube.ui.viewmodel.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce


abstract class AubeViewModel<View>(application: Application) : AndroidViewModel(application) {

    private val callbacks = mutableSetOf<Pair<LifecycleOwner, View>>()

    override fun onCleared() {
        callbacks.clear()
    }

    protected fun execute(
        onError: suspend (e: Exception) -> Unit = {},
        request: suspend (c: CoroutineScope) -> Unit
    ): Job = viewModelScope.launch {
        try {
            request(this)
        } catch (e: Exception) {
            onError(e)
        }
    }

    fun addCallbackView(lifecycleOwner: LifecycleOwner, view: View): Boolean =
        callbacks.add(Pair(lifecycleOwner, view))

    fun detachCallbackView(lifecycleOwner: LifecycleOwner): Boolean =
        callbacks.find { it.first == lifecycleOwner }?.let {
            callbacks.remove(it)
        } ?: false

    fun detachCallbackView(view: View): Boolean =
        callbacks.find { it.second == view }?.let {
            callbacks.remove(it)
        } ?: false

    protected suspend fun callView(action: (View) -> Unit) {
        withContext(Dispatchers.Main) {
            produce {
                for (cv in callbacks) {
                    if (cv.first.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                        send(cv.second)
                    }
                }
            }.consumeEach(action)
        }
    }
}