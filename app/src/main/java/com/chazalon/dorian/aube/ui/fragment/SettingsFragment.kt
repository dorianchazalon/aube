package com.chazalon.dorian.aube.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import com.chazalon.dorian.aube.databinding.FragmentSettingsBinding
import com.chazalon.dorian.aube.utils.AudioUtils
import com.chazalon.dorian.aube.utils.SettingsUtils


class SettingsFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews() {
        with(binding.delay) {
            minValue = 1
            maxValue = 20
            value = SettingsUtils.getSnoozeDelay(requireContext())
            setOnValueChangedListener { _, _, newVal ->
                SettingsUtils.setSnoozeDelay(
                    requireContext(),
                    newVal
                )
            }
        }

        with(binding.hasDefaultVolume) {
            isChecked = SettingsUtils.useDefaultVolume(requireContext())
            setOnCheckedChangeListener { _, checked ->
                binding.volumeBar.isEnabled = checked
                SettingsUtils.setUseDefaultVolume(requireContext(), checked)
            }
        }

        with(binding.volumeBar) {
            max = AudioUtils.getMax(requireContext())
            progress = SettingsUtils.getDefaultVolume(requireContext())
            setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    SettingsUtils.setDefaultVolume(requireContext(), progress)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }
            })
        }

        with(binding.turnOffVolume) {
            isChecked = SettingsUtils.turnOffAutomatically(requireContext())
            setOnCheckedChangeListener { _, checked ->
                SettingsUtils.setTurnOffAutomatically(
                    requireContext(),
                    checked
                )
            }
        }
    }
}
