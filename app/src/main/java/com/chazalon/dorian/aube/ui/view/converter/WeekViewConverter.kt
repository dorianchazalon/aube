package com.chazalon.dorian.aube.ui.view.converter

import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.ui.view.DayItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.utils.Converter


class WeekViewConverter : Converter<List<Day>, WeekItemView> {
    override fun convert(obj: List<Day>): WeekItemView {

        val days = ArrayList<DayItemView>()

        obj.forEach {
            when (it) {
                Day.MONDAY -> days.add(DayItemView.MONDAY)
                Day.TUESDAY -> days.add(DayItemView.TUESDAY)
                Day.WEDNESDAY -> days.add(DayItemView.WEDNESDAY)
                Day.THURSDAY -> days.add(DayItemView.THURSDAY)
                Day.FRIDAY -> days.add(DayItemView.FRIDAY)
                Day.SATURDAY -> days.add(DayItemView.SATURDAY)
                Day.SUNDAY -> days.add(DayItemView.SUNDAY)
            }
        }

        return WeekItemView(days.toList())
    }
}