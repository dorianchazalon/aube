package com.chazalon.dorian.aube.ui.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.data.repository.InfoSpotifyRepository
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.domain.model.Playlist
import com.chazalon.dorian.aube.domain.usecase.CreateAlarmUseCase
import com.chazalon.dorian.aube.domain.usecase.InfoMusicUseCase
import com.chazalon.dorian.aube.ui.extension.buildList
import com.chazalon.dorian.aube.ui.view.CreateAlarmItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.ui.view.converter.PlaylistItemViewConverter
import com.chazalon.dorian.aube.ui.view.converter.WeekConverter
import com.chazalon.dorian.aube.ui.viewmodel.base.AubeViewModel
import com.chazalon.dorian.aube.utils.Converter


class CreateAlarmViewModel(
    application: Application,
    private val infoMusicUseCase: InfoMusicUseCase,
    private val createAlarmUseCase: CreateAlarmUseCase
) : AubeViewModel<CreateAlarmViewModel.CreateAlarmCallbackView>(application) {

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CreateAlarmViewModel::class.java)) {
                val infoMusicUseCase = InfoMusicUseCase(InfoSpotifyRepository.getInstance())
                val createAlarmUseCase = CreateAlarmUseCase(AlarmDataRepository.getInstance())

                return CreateAlarmViewModel(application, infoMusicUseCase, createAlarmUseCase) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    private val _playlists = MutableLiveData<List<Playlist>>()

    val playlists = _playlists.buildList(PlaylistItemViewConverter())

    fun connect() {
        execute(
            onError = {
                callView { it.onMusicConnectionError() }
            }
        ) {
            infoMusicUseCase.connect()
            callView { it.onMusicConnected() }
        }
    }

    fun playlists() {
        execute(
            onError = {
                callView { it.onPlaylistError() }
            }
        ) {
            callView { it.onPlaylistLoad(true) }
            _playlists.value = infoMusicUseCase.playlists()
            callView { it.onPlaylistLoad(false) }
        }
    }

    fun createAlarm(itemView: CreateAlarmItemView) {
        val converter: Converter<WeekItemView, List<Day>> = WeekConverter()
        execute(
            onError = {
                callView { it.onCreateAlarmError() }
            }
        ) {
            createAlarmUseCase.createAlarm(
                itemView.title, converter.convert(itemView.days), itemView.hour, itemView.minute,
                itemView.useCustomVolume, itemView.volume, itemView.playlistId
            )
            callView { it.onCreateAlarmSuccess() }
        }
    }

    interface CreateAlarmCallbackView {
        // Music connect
        fun onMusicConnected()
        fun onMusicConnectionError()

        // Playlist
        fun onPlaylistLoad(loading: Boolean)
        fun onPlaylistError()

        // Alarm creation
        fun onCreateAlarmSuccess()
        fun onCreateAlarmError()
    }
}