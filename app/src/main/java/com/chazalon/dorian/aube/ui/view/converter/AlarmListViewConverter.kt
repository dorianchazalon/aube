package com.chazalon.dorian.aube.ui.view.converter

import android.content.Context
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.ui.view.AlarmListItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.utils.Converter
import java.text.SimpleDateFormat
import java.util.*


class AlarmListViewConverter(private val context: Context) : Converter<Alarm, AlarmListItemView> {

    private val weekItemConverter: Converter<List<Day>, WeekItemView> = WeekViewConverter()

    override fun convert(obj: Alarm): AlarmListItemView {
        val c = Calendar.getInstance()
        c.timeInMillis = obj.lastTriggerTime ?: 0
        val format = SimpleDateFormat("d MMMM '-' HH:mm:ss", Locale.FRANCE)

        return AlarmListItemView(
            obj.id,
            obj.title,
            context.getString(R.string.hour_h, obj.hour, obj.minute),
            obj.active,
            weekItemConverter.convert(obj.days),
            format.format(c.time)
        )
    }
}