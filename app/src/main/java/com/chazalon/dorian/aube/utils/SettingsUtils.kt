package com.chazalon.dorian.aube.utils

import android.content.Context


class SettingsUtils {

    companion object {
        private const val SETTINGS_FILE = "settings_prefs"

        private const val KEY_DELAY = "key_snooze_delay"
        private const val KEY_USE_DEFAULT_VOLUME = "key_use_default_volume"
        private const val KEY_DEFAULT_VOLUME = "key_default_volume"
        private const val KEY_TURN_OFF_AUTOMATICALLY = "key_turn_off_automatically"
        private const val KEY_HACK_DONE = "key_hack_done"

        // Valeurs par défaut
        private const val DEFAULT_DELAY = 10
        private const val DEFAULT_USE_DEFAULT_VOLUME = true
        private const val DEFAULT_VOLUME = 3
        private const val DEFAULT_TURN_OFF_AUTOMATICALLY = true

        fun getSnoozeDelay(context: Context): Int {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            return sharedPref.getInt(KEY_DELAY, DEFAULT_DELAY)
        }

        fun setSnoozeDelay(context: Context, delay: Int) {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt(KEY_DELAY, delay)
            editor.apply()
        }

        fun getDefaultVolume(context: Context): Int {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            return sharedPref.getInt(KEY_DEFAULT_VOLUME, DEFAULT_VOLUME)
        }

        fun setDefaultVolume(context: Context, volume: Int) {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt(KEY_DEFAULT_VOLUME, volume)
            editor.apply()
        }

        fun useDefaultVolume(context: Context): Boolean {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            return sharedPref.getBoolean(KEY_USE_DEFAULT_VOLUME, DEFAULT_USE_DEFAULT_VOLUME)
        }

        fun setUseDefaultVolume(context: Context, useDefaultVolume: Boolean) {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putBoolean(KEY_USE_DEFAULT_VOLUME, useDefaultVolume)
            editor.apply()
        }

        fun turnOffAutomatically(context: Context): Boolean {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            return sharedPref.getBoolean(KEY_TURN_OFF_AUTOMATICALLY, DEFAULT_TURN_OFF_AUTOMATICALLY)
        }

        fun setTurnOffAutomatically(context: Context, turnOff: Boolean) {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putBoolean(KEY_TURN_OFF_AUTOMATICALLY, turnOff)
            editor.apply()
        }

        fun isHackDone(context: Context): Boolean {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            return sharedPref.getBoolean(KEY_HACK_DONE, false)
        }

        fun hackDone(context: Context) {
            val sharedPref = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putBoolean(KEY_HACK_DONE, true)
            editor.apply()
        }
    }
}