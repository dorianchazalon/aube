package com.chazalon.dorian.aube.ui.view


data class AlarmListItemView(
    val id: Int,
    val title: String,
    val time: String,
    val active: Boolean,
    val weekItemView: WeekItemView,
    val lastTriggerTime: String
)