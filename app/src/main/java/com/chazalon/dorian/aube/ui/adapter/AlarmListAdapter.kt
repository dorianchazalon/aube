package com.chazalon.dorian.aube.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chazalon.dorian.aube.databinding.RowAlarmBinding
import com.chazalon.dorian.aube.ui.view.AlarmListItemView


class AlarmListAdapter : RecyclerView.Adapter<AlarmListAdapter.Companion.AlarmViewHolder>() {

    var itemViews: List<AlarmListItemView>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var clickListener: OnItemClickListener? = null

    override fun getItemCount(): Int = itemViews?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmViewHolder {
        val binding = RowAlarmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AlarmViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AlarmViewHolder, position: Int) {
        val data = itemViews!![position]

        with(holder.binding) {
            time.text = data.time
            title.text = data.title
            active.isChecked = data.active
            weekLayout.setItems(data.weekItemView)

            active.setOnCheckedChangeListener { _, checked ->
                clickListener?.onAlarmActiveChanged(
                    data.copy(active = checked)
                )
            }
            root.setOnClickListener { clickListener?.onAlarmClicked(data.id) }
            root.setOnLongClickListener {
                clickListener?.onAlarmLongClicked(data.lastTriggerTime) ?: false
            }
        }
    }

    interface OnItemClickListener {
        fun onAlarmClicked(id: Int)
        fun onAlarmLongClicked(lastTrigger: String): Boolean
        fun onAlarmActiveChanged(itemView: AlarmListItemView)
    }

    companion object {
        class AlarmViewHolder(val binding: RowAlarmBinding) : RecyclerView.ViewHolder(binding.root)
    }
}