package com.chazalon.dorian.aube.data.source

//TODO: il faudrait duppliquer ce fichier pour chaque buildType et/ou flavor (debug/preprod/release) pour différencer les environnements de prod et preprod

// Spotify Service
const val SPOTIFY_BASE_URL = "https://api.spotify.com/v1/"

// Spotify SDK
const val SPOTIFY_CLIENT_ID = "37ac909ef8b94dfbb60751a046dd3fab"
const val SPOTIFY_REDIRECT = "dorianchazalon://app_callback"