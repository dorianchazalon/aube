package com.chazalon.dorian.aube.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.domain.usecase.EditAlarmUseCase
import com.chazalon.dorian.aube.ui.view.AlarmListItemView
import com.chazalon.dorian.aube.ui.viewmodel.base.AubeViewModel


class AlarmListViewModel(
    application: Application,
    private val editAlarmUseCase: EditAlarmUseCase
) : AubeViewModel<Nothing>(application) {

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AlarmListViewModel::class.java)) {
                val editAlarmUseCase = EditAlarmUseCase(AlarmDataRepository.getInstance())

                return AlarmListViewModel(application, editAlarmUseCase) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    fun editAlarmActive(itemView: AlarmListItemView) {
        execute {
            editAlarmUseCase.editActive(itemView.id, itemView.active)
        }
    }
}