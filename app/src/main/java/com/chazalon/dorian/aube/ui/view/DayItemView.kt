package com.chazalon.dorian.aube.ui.view


enum class DayItemView {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
