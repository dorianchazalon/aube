package com.chazalon.dorian.aube.data.repository

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chazalon.dorian.aube.data.model.PlaylistEntity
import com.chazalon.dorian.aube.data.model.converter.PlaylistConverter
import com.chazalon.dorian.aube.data.source.SPOTIFY_CLIENT_ID
import com.chazalon.dorian.aube.data.source.SPOTIFY_REDIRECT
import com.chazalon.dorian.aube.data.source.music.SpotifyService
import com.chazalon.dorian.aube.domain.model.Playlist
import com.chazalon.dorian.aube.domain.repository.InfoMusicRepository
import com.chazalon.dorian.aube.utils.Converter
import com.spotify.sdk.android.auth.AuthorizationClient
import com.spotify.sdk.android.auth.AuthorizationRequest
import com.spotify.sdk.android.auth.AuthorizationResponse
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


private const val ACTIVITY_REQUEST_CODE = 1655

class InfoSpotifyRepository
private constructor(private val application: Application) : InfoMusicRepository {

    private val playlistConverter: Converter<PlaylistEntity, Playlist> = PlaylistConverter()

    override suspend fun connect() {
        // On crée une coroutine en récupérant le Continuation pour indiquer quand on en a terminé avec le processus
        suspendCoroutine<String> {
            connectContinuation = it
            application.startActivity(LoginCatchActivity.buildIntent(application))
        }
    }

    override suspend fun playlists(): List<Playlist> =
        if (token != null) playlistConverter.convert(
            SpotifyService.getService(token!!).playlists().playlists
        )
        else throw Exception("invalid token")


    /**
     * Classe intermédiaire servant à lancer celle de Spotify et réceptionner son resultat avec on onActivityResult <br/>
     * Ne pas lancer en dehors du {@link InfoSpotifyRepository}
     */
    class LoginCatchActivity : AppCompatActivity() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            val builder =
                AuthorizationRequest.Builder(
                    SPOTIFY_CLIENT_ID,
                    AuthorizationResponse.Type.TOKEN,
                    SPOTIFY_REDIRECT
                )
            builder.setScopes(arrayListOf("streaming").toTypedArray())

            AuthorizationClient.openLoginActivity(this, ACTIVITY_REQUEST_CODE, builder.build())
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            when (requestCode) {
                ACTIVITY_REQUEST_CODE -> {
                    val response = AuthorizationClient.getResponse(resultCode, data)
                    when (response?.type) {
                        AuthorizationResponse.Type.TOKEN -> {
                            token = response.accessToken
                            // Une fois que l'Activity de Spotify nous a retourné le token, on peut signaler à notre coroutine en attente que c'est bon (avec le token) ou si c'est une erreur
                            if (response.accessToken.isNotBlank()) {
                                connectContinuation?.resume(response.accessToken)
                            } else {
                                connectContinuation?.resumeWithException(Exception())
                            }
                        }
                        else -> {
                            connectContinuation?.resumeWithException(Exception())
                        }
                    }
                    connectContinuation = null
                    finish()
                    overridePendingTransition(0, 0)
                }
            }
        }

        companion object {
            fun buildIntent(context: Context): Intent {
                val i = Intent(context, LoginCatchActivity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                return i
            }
        }
    }

    companion object {
        private var instance: InfoSpotifyRepository? = null

        private var token: String? = null
        private var connectContinuation: Continuation<String>? =
            null // Permet d'attendre un résultat de connexion

        fun init(application: Application) {
            if (instance == null) {
                instance = InfoSpotifyRepository(application)
            }
        }

        fun getInstance(): InfoSpotifyRepository {
            return instance!!
        }
    }
}