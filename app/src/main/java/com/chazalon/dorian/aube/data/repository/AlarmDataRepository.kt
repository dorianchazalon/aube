package com.chazalon.dorian.aube.data.repository

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.os.bundleOf
import androidx.navigation.NavDeepLinkBuilder
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.data.model.AlarmEntity
import com.chazalon.dorian.aube.data.model.converter.AlarmConverter
import com.chazalon.dorian.aube.data.source.alarm.AubeDatabase
import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.domain.repository.AlarmRepository
import com.chazalon.dorian.aube.ui.receiver.RingReceiver
import com.chazalon.dorian.aube.utils.Converter
import java.util.*


class AlarmDataRepository
private constructor(private val application: Application) : AlarmRepository {

    private val converter: Converter<AlarmEntity, Alarm> = AlarmConverter()

    override suspend fun allAlarms(): List<Alarm> =
        converter.convert(AubeDatabase.db().alarmDao().allAlarms())

    override suspend fun alarm(id: Int): Alarm =
        converter.convert(AubeDatabase.db().alarmDao().alarm(id))

    override suspend fun insertOrReplaceAlarm(
        title: String, days: List<Day>, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?, active: Boolean
    ): Alarm {
        val id = AubeDatabase.db().alarmDao().insertOrReplaceAlarm(
            AlarmEntity(
                null, title, active, Day.convertDaysToBytes(days), hour, minute,
                useCustomVolume, volume, null, playlistId
            )
        ).toInt()

        val alarm = alarm(id)
        if (alarm.active) {
            createSystemAlarm(
                application,
                id,
                getNextTimeToRing(Calendar.getInstance(), alarm).timeInMillis
            )
        }

        return alarm
    }

    override suspend fun updateAlarm(
        id: Int, title: String, days: List<Day>, hour: Int, minute: Int,
        useCustomVolume: Boolean, volume: Int, playlistId: String?
    ) {
        AubeDatabase.db().alarmDao()
            .updateAlarm(
                id,
                title,
                Day.convertDaysToBytes(days),
                hour,
                minute,
                useCustomVolume,
                volume,
                playlistId
            )
        val alarm = alarm(id)
        removeSystemAlarm(application, alarm.id)
        if (alarm.active) {
            createSystemAlarm(
                application,
                alarm.id,
                getNextTimeToRing(Calendar.getInstance(), alarm).timeInMillis
            )
        }
    }

    override suspend fun updateAlarmActive(id: Int, active: Boolean) {
        AubeDatabase.db().alarmDao().updateAlarmActive(id, active)
        val alarm = alarm(id)
        removeSystemAlarm(application, id)
        if (active) {
            createSystemAlarm(
                application,
                id,
                getNextTimeToRing(Calendar.getInstance(), alarm).timeInMillis
            )
        }
    }

    override suspend fun updateAlarmLastTriggerTime(id: Int, lastTriggerTime: Long) {
        AubeDatabase.db().alarmDao().updateAlarmLastTriggerTime(id, lastTriggerTime)
    }

    override suspend fun snoozeAlarm(id: Int, delay: Int) {
        val later = Calendar.getInstance()
        later.add(Calendar.MINUTE, delay)
        createSystemAlarm(application, id, later.timeInMillis)
    }

    override suspend fun delete(id: Int) {
        AubeDatabase.db().alarmDao().deleteAlarm(id)
        removeSystemAlarm(application, id)
    }

    override suspend fun createNextDayAlarm(id: Int) {
        val alarm = alarm(id)
        removeSystemAlarm(application, id)
        createSystemAlarm(
            application,
            id,
            getNextTimeToRing(Calendar.getInstance(), alarm).timeInMillis
        )
    }

    companion object {

        private var instance: AlarmDataRepository? = null

        fun init(application: Application) {
            if (instance == null) {
                instance = AlarmDataRepository(application)
            }
        }

        fun getInstance(): AlarmDataRepository {
            return instance!!
        }

        private fun createSystemAlarm(context: Context, id: Int, time: Long) {
            val alarmManager = context.getSystemService(AlarmManager::class.java)

            val info = AlarmManager.AlarmClockInfo(time, createEditAlarmPendingIntent(context, id))

            alarmManager!!.setAlarmClock(info, createReceiverPendingIntent(context, id))
        }

        private fun removeSystemAlarm(context: Context, id: Int) {
            val alarmManager = context.getSystemService(AlarmManager::class.java)

            alarmManager!!.cancel(createReceiverPendingIntent(context, id))
        }

        private fun createReceiverPendingIntent(context: Context, id: Int): PendingIntent {
            val ringIntent = Intent(context, RingReceiver::class.java)
            ringIntent.putExtra(RingReceiver.EXTRA_ALARM_ID, id)

            return PendingIntent.getBroadcast(context, id, ringIntent, 0)
        }

        private fun createEditAlarmPendingIntent(context: Context, id: Int): PendingIntent {
            return NavDeepLinkBuilder(context)
                .setGraph(R.navigation.navigation_main)
                .setDestination(R.id.editAlarmFragment)
                .setArguments(bundleOf(context.getString(R.string.arg_alarm_id) to id))
                .createPendingIntent()
        }

        /**
         * @param origin date de repère pour savoir le jour suivant à partir de cette dite date
         * @param alarm l'alarme qu'on doit vérifier
         */
        fun getNextTimeToRing(origin: Calendar, alarm: Alarm): Calendar {
            // On crée la prochaine date sans toucher à la date d'origine pour toujours avoir cette dernière en mémoire
            val time = origin.clone() as Calendar

            // On met la bonne heure de sonnerie
            time.set(Calendar.HOUR_OF_DAY, alarm.hour)
            time.set(Calendar.MINUTE, alarm.minute)
            time.set(Calendar.SECOND, 0)
            time.set(Calendar.MILLISECOND, 0)

            // Si à cause de la nouvelle heure on se retrouve dans le passé (donc une heure de la journée déjà passée), on passe au jour suivant car impossible que l'alarme puisse sonner ce jour-ci
            // exemple: si on est en train de régler une alarme pour 7h30 tous les matins à 10h00 dans la journée
            if (time < origin) {
                time.add(Calendar.DATE, 1)
            }

            // On récupère les jours où l'alarme doit sonner
            val days = alarm.days
            // On cherche le jour suivant à sonner en ajoutant à la date de résultat un jour tant qu'on tombe pas sur un jour de la semaine de sonnerie
            while (!days.contains(getCalendarDay(time.get(Calendar.DAY_OF_WEEK)))) {
                time.add(Calendar.DATE, 1)
            }

            return time
        }

        private fun getCalendarDay(day: Int): Day =
            when (day) {
                Calendar.MONDAY -> Day.MONDAY
                Calendar.TUESDAY -> Day.TUESDAY
                Calendar.WEDNESDAY -> Day.WEDNESDAY
                Calendar.THURSDAY -> Day.THURSDAY
                Calendar.FRIDAY -> Day.FRIDAY
                Calendar.SATURDAY -> Day.SATURDAY
                Calendar.SUNDAY -> Day.SUNDAY
                else -> Day.MONDAY
            }
    }
}