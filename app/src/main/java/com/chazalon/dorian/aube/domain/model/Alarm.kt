package com.chazalon.dorian.aube.domain.model


data class Alarm(
    val id: Int,
    val title: String,
    val active: Boolean,
    val days: List<Day>,
    val hour: Int,
    val minute: Int,
    val useCustomVolume: Boolean,
    val volume: Int,
    val lastTriggerTime: Long?,
    val playlistId: String?
)