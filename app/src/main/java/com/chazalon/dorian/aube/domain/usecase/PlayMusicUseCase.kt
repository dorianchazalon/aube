package com.chazalon.dorian.aube.domain.usecase

import com.chazalon.dorian.aube.domain.model.MusicState
import com.chazalon.dorian.aube.domain.repository.PlayMusicRepository
import kotlinx.coroutines.channels.SendChannel


class PlayMusicUseCase(private val playMusicRepository: PlayMusicRepository) {

    suspend fun connect(silent: Boolean) = playMusicRepository.connect(silent)

    suspend fun disconnect() = playMusicRepository.disconnect()

    suspend fun playMusic(playlistId: String) = playMusicRepository.play(playlistId)

    suspend fun skipMusic() = playMusicRepository.skip()

    suspend fun pauseMusic() = playMusicRepository.pause()

    fun musicEvents(channel: SendChannel<MusicState>) =
        playMusicRepository.registerMusicEvents(channel)
}