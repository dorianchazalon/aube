package com.chazalon.dorian.aube.data.repository

import android.app.Application
import android.util.Log
import com.chazalon.dorian.aube.data.model.converter.SpotifyMusicStateConverter
import com.chazalon.dorian.aube.data.source.SPOTIFY_CLIENT_ID
import com.chazalon.dorian.aube.data.source.SPOTIFY_REDIRECT
import com.chazalon.dorian.aube.domain.model.MusicState
import com.chazalon.dorian.aube.domain.repository.PlayMusicRepository
import com.chazalon.dorian.aube.utils.Converter
import com.spotify.android.appremote.api.ConnectionParams
import com.spotify.android.appremote.api.Connector
import com.spotify.android.appremote.api.SpotifyAppRemote
import com.spotify.protocol.client.CallResult
import com.spotify.protocol.types.PlayerState
import com.spotify.protocol.types.Repeat
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.SendChannel
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


class PlaySpotifyRepository
private constructor(private val application: Application) : PlayMusicRepository {

    private var spotifyAppRemote: SpotifyAppRemote? = null
    private val stateConverter: Converter<PlayerState, MusicState> = SpotifyMusicStateConverter()

    private val scope = CoroutineScope(Dispatchers.IO)
    private var musicStateChannel: SendChannel<MusicState>? = null

    private var currentState: MusicState? = null


    override suspend fun connect(silent: Boolean) {
        // On crée une coroutine qu'on suspend tant qu'on a pas eu le callback de Spotify
        suspendCoroutine<Boolean> { continuation ->
            val connectionParams = ConnectionParams.Builder(SPOTIFY_CLIENT_ID)
                .setRedirectUri(SPOTIFY_REDIRECT).showAuthView(!silent).build()

            SpotifyAppRemote.connect(
                application,
                connectionParams,
                object : Connector.ConnectionListener {
                    override fun onFailure(error: Throwable) {
                        Log.e("spotify", "ERROR : ${error.message}", error)
                        continuation.resumeWithException(error)
                    }

                    override fun onConnected(spotifyAppRemote: SpotifyAppRemote) {
                        Log.d("spotify", "connected !")
                        this@PlaySpotifyRepository.spotifyAppRemote = spotifyAppRemote
                        callUntilSuccess { this@PlaySpotifyRepository.spotifyAppRemote?.connectApi?.connectSwitchToLocalDevice() }
                        this@PlaySpotifyRepository.spotifyAppRemote?.playerApi?.subscribeToPlayerState()
                            ?.setEventCallback { playerState ->
                                musicStateChannel?.run {
                                    val newState = stateConverter.convert(playerState)
                                    if (newState != currentState) {
                                        currentState = newState.copy()
                                        scope.launch {
                                            send(newState)
                                        }
                                    }
                                }
                            }
                        continuation.resume(true)
                    }
                })
        }
    }

    override suspend fun disconnect() {
        withContext(Dispatchers.Main) {
            SpotifyAppRemote.disconnect(spotifyAppRemote)
        }
    }

    override suspend fun play(playlistId: String) {
        withContext(Dispatchers.Main) {
            if (spotifyAppRemote == null || spotifyAppRemote?.isConnected == false) {
                throw Exception("Spotify repo not connected")
            }

            spotifyAppRemote?.playerApi?.run {
                setRepeat(Repeat.OFF)
                setShuffle(true)

                play("spotify:playlist:$playlistId")
                    ?.setErrorCallback { error ->
                        if (error.cause != null) {
                            throw error.cause!!
                        }
                    }
            }
        }
    }

    override suspend fun skip() {
        withContext(Dispatchers.Main) {
            spotifyAppRemote?.playerApi?.skipNext()?.setErrorCallback { error ->
                if (error.cause != null) {
                    throw error.cause!!
                }
            }
        }
    }

    override suspend fun pause() {
        withContext(Dispatchers.Main) {
            spotifyAppRemote?.playerApi?.pause()
                ?.setErrorCallback { error ->
                    if (error.cause != null) {
                        throw error.cause!!
                    }
                }
        }
    }

    override fun registerMusicEvents(channel: SendChannel<MusicState>) {
        this.musicStateChannel = channel
    }

    /**
     * Exécute le block donné en boucle jusqu'à ce que ce dernier fonctionne
     */
    private fun <T> callUntilSuccess(block: () -> CallResult<T>?) {
        block()?.setErrorCallback { callUntilSuccess(block) }
    }

    companion object {
        private var instance: PlaySpotifyRepository? = null

        fun init(application: Application) {
            if (instance == null) {
                instance = PlaySpotifyRepository(application)
            }
        }

        fun getInstance(): PlaySpotifyRepository {
            return instance!!
        }
    }
}