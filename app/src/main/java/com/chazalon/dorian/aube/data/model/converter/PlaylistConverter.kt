package com.chazalon.dorian.aube.data.model.converter

import com.chazalon.dorian.aube.data.model.PlaylistEntity
import com.chazalon.dorian.aube.domain.model.Playlist
import com.chazalon.dorian.aube.utils.Converter


class PlaylistConverter : Converter<PlaylistEntity, Playlist> {
    override fun convert(obj: PlaylistEntity): Playlist {
        return Playlist(obj.id, obj.name, obj.public, obj.getTotal(), obj.getImage())
    }
}