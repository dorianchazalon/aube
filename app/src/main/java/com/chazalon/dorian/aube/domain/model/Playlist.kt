package com.chazalon.dorian.aube.domain.model


data class Playlist(
    val id: String,
    val title: String,
    val public: Boolean,
    val total: Int,
    val image: String?
)