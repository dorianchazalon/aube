package com.chazalon.dorian.aube.ui.view


data class PlaylistItemView(
    val id: String,
    val title: String,
    val total: Int,
    val image: String?,
    var selected: Boolean = false
)