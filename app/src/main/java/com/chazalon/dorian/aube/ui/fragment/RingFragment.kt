package com.chazalon.dorian.aube.ui.fragment


import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.chazalon.dorian.aube.databinding.FragmentRingBinding
import com.chazalon.dorian.aube.ui.service.RingService
import com.chazalon.dorian.aube.ui.view.MusicStateItemView

class RingFragment : Fragment() {

    private var service: RingService? = null

    // Permet le dialogue entre le RingService et le RingFragment
    // Pour afficher des infos (RingService -> RingFragment) ou donner des instructions à effectuer (RingFragment -> RingService)
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            this@RingFragment.service = (service as RingService.RingBinder).getService()
            this@RingFragment.service?.ringCallback = ringCallback
            this@RingFragment.service?.currentState?.let { binding.track.text = it.track }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            this@RingFragment.service = null
        }
    }

    private lateinit var binding: FragmentRingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().bindService(
            Intent(requireContext(), RingService::class.java),
            connection,
            Context.BIND_AUTO_CREATE or Context.BIND_IMPORTANT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRingBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unbindService(connection)
    }


    private fun initViews() {
        // Les post delayed ne sont pas nécessaires, c'est parce que je trouve ça plus stylé
        binding.snooze.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(
                { service?.snooze() },
                500
            )
        }
        binding.dismiss.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(
                { service?.dismiss() },
                500
            )
        }
        binding.skip.setOnClickListener { service?.skip() }
    }

    private val ringCallback = object : RingService.RingCallback {
        override fun onStateChanged(state: MusicStateItemView) {
            binding.track.text = state.track
        }

        override fun onSnoozed() {
            requireActivity().finish()
        }

        override fun onDismissed() {
            requireActivity().finish()
        }
    }
}
