package com.chazalon.dorian.aube.ui.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chazalon.dorian.aube.data.repository.AlarmDataRepository
import com.chazalon.dorian.aube.data.repository.InfoSpotifyRepository
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.domain.model.Playlist
import com.chazalon.dorian.aube.domain.usecase.EditAlarmUseCase
import com.chazalon.dorian.aube.domain.usecase.InfoMusicUseCase
import com.chazalon.dorian.aube.ui.extension.buildList
import com.chazalon.dorian.aube.ui.view.EditAlarmItemView
import com.chazalon.dorian.aube.ui.view.WeekItemView
import com.chazalon.dorian.aube.ui.view.converter.PlaylistItemViewConverter
import com.chazalon.dorian.aube.ui.view.converter.WeekConverter
import com.chazalon.dorian.aube.ui.viewmodel.base.AubeViewModel
import com.chazalon.dorian.aube.utils.Converter


class EditAlarmViewModel(
    application: Application,
    private val infoMusicUseCase: InfoMusicUseCase,
    private val editAlarmUseCase: EditAlarmUseCase
) : AubeViewModel<EditAlarmViewModel.EditAlarmCallbackView>(application) {

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(EditAlarmViewModel::class.java)) {
                val infoMusicUseCase = InfoMusicUseCase(InfoSpotifyRepository.getInstance())
                val editAlarmUseCase = EditAlarmUseCase(AlarmDataRepository.getInstance())

                return EditAlarmViewModel(application, infoMusicUseCase, editAlarmUseCase) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    private val _playlists = MutableLiveData<List<Playlist>>()

    val playlists = _playlists.buildList(PlaylistItemViewConverter())

    fun connect() {
        execute(
            onError = {
                callView { it.onMusicConnectionError() }
            }
        ) {
            infoMusicUseCase.connect()
            callView { it.onMusicConnected() }
        }
    }

    fun playlists() {
        execute(
            onError = {
                callView { it.onPlaylistError() }
            }
        ) {
            callView { it.onPlaylistLoad(true) }
            _playlists.value = infoMusicUseCase.playlists()
            callView { it.onPlaylistLoad(false) }
        }
    }

    fun update(itemView: EditAlarmItemView) {
        val converter: Converter<WeekItemView, List<Day>> = WeekConverter()
        execute(
            onError = { error ->
                callView { it.onEditAlarmError(error.message) }
            }
        ) {
            callView { it.onEditAlarmLoad(true) }
            editAlarmUseCase.editAlarm(
                itemView.id,
                itemView.title,
                converter.convert(itemView.days),
                itemView.hour,
                itemView.minute,
                itemView.useCustomVolume,
                itemView.volume,
                itemView.playlistId
            )
            callView { it.onEditAlarmLoad(false) }
            callView { it.onEditAlarmSuccess() }
        }
    }

    fun delete(id: Int) {
        execute(
            onError = { error ->
                callView { it.onEditAlarmError(error.message) }
            }
        ) {
            callView { it.onEditAlarmLoad(true) }
            editAlarmUseCase.deleteAlarm(id)
            callView { it.onEditAlarmLoad(false) }
            callView { it.onEditAlarmSuccess() }
        }
    }

    interface EditAlarmCallbackView {
        // Music connect
        fun onMusicConnected()
        fun onMusicConnectionError()

        // Playlist
        fun onPlaylistLoad(loading: Boolean)
        fun onPlaylistError()

        // Alarm edition
        fun onEditAlarmLoad(loading: Boolean)
        fun onEditAlarmSuccess()
        fun onEditAlarmError(message: String?)
    }
}