package com.chazalon.dorian.aube.ui.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.chazalon.dorian.aube.ui.service.RingService


/**
 * BroadcastReceiver qui est appelé quand une alarme système se déclenche
 */
class RingReceiver : BroadcastReceiver() {

    companion object {
        const val EXTRA_ALARM_ID = "EXTRA_ALARM_ID"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val id = intent?.getIntExtra(EXTRA_ALARM_ID, -1) ?: -1
        Log.d("RingReceiver", "onReceive Ring broadcast id $id")

        // On lance le RingService qui va s'occuper de faire tourner le réveil (musique ou sonnerie) en background
        context?.run {
            startForegroundService(RingService.buildStartIntent(this, id))
        }
    }
}