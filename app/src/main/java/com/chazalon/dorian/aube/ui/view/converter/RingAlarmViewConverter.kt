package com.chazalon.dorian.aube.ui.view.converter

import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.ui.view.RingAlarmItemView
import com.chazalon.dorian.aube.utils.Converter


class RingAlarmViewConverter : Converter<Alarm, RingAlarmItemView> {

    override fun convert(obj: Alarm): RingAlarmItemView {
        return RingAlarmItemView(
            obj.id,
            obj.title,
            obj.useCustomVolume,
            obj.volume,
            obj.playlistId
        )
    }
}