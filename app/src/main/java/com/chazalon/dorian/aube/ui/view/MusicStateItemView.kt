package com.chazalon.dorian.aube.ui.view


data class MusicStateItemView(
    val track: String
)