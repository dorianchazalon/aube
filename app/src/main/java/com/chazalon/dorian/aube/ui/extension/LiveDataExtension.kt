package com.chazalon.dorian.aube.ui.extension

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.chazalon.dorian.aube.utils.Converter


fun <M, IV> LiveData<M>.buildDetail(converter: Converter<M, IV>): LiveData<IV> =
    Transformations.map(this) {
        converter.convert(it)
    }

fun <M, IV> LiveData<List<M>>.buildList(converter: Converter<M, IV>): LiveData<List<IV>> =
    Transformations.map(this) {
        converter.convert(it)
    }

fun <M> MutableLiveData<M>.read(): LiveData<M> = this

fun <M, I> MutableLiveData<M>.selectDetailFromList(
    list: MutableLiveData<List<M>>,
    id: I, find: (model: M, id: I) -> Boolean
) {
    list.value?.find { model ->
        find(model, id)
    }?.let { model ->
        this.value = model
    }
}