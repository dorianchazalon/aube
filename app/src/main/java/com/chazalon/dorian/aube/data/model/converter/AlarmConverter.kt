package com.chazalon.dorian.aube.data.model.converter

import com.chazalon.dorian.aube.data.model.AlarmEntity
import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.model.Day
import com.chazalon.dorian.aube.utils.Converter


class AlarmConverter : Converter<AlarmEntity, Alarm> {
    override fun convert(obj: AlarmEntity): Alarm {
        return Alarm(
            obj.id!!,
            obj.title,
            obj.active,
            Day.convertBytesToDays(obj.days),
            obj.hour,
            obj.minute,
            obj.useCustomVolume,
            obj.volume,
            obj.lastTriggerTime,
            obj.playlistId
        )
    }
}