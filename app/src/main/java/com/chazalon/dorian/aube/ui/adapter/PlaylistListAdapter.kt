package com.chazalon.dorian.aube.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.RecyclerView
import com.chazalon.dorian.aube.R
import com.chazalon.dorian.aube.databinding.RowPlaylistBinding
import com.chazalon.dorian.aube.ui.extension.invisible
import com.chazalon.dorian.aube.ui.view.PlaylistItemView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso


class PlaylistListAdapter :
    RecyclerView.Adapter<PlaylistListAdapter.Companion.PlaylistViewHolder>() {

    var itemViews: List<PlaylistItemView>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = itemViews?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val binding = RowPlaylistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlaylistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val data = itemViews!![position]

        with(holder.binding) {
            Picasso.get().load(data.image).into(image, object : Callback {
                override fun onSuccess() {
                }

                override fun onError(e: Exception?) {
                    image.invisible()
                }
            })

            title.text = data.title
            total.text = root.context.resources.getQuantityString(
                R.plurals.total_playlist,
                data.total,
                data.total
            )

            updateCheck(parentLayout, data.selected)

            root.setOnClickListener {
                itemViews?.forEach { data -> data.selected = false }
                data.selected = !data.selected
                notifyDataSetChanged()
            }
        }
    }

    fun getSelectedPlaylist(): PlaylistItemView? {
        itemViews?.forEach {
            if (it.selected) return it
        }

        return null
    }

    companion object {
        class PlaylistViewHolder(val binding: RowPlaylistBinding) :
            RecyclerView.ViewHolder(binding.root)

        fun updateCheck(layout: LinearLayout, selected: Boolean) {
            layout.background =
                if (selected) ContextCompat.getColor(layout.context, R.color.colorAccent)
                    .toDrawable()
                else ContextCompat.getColor(layout.context, android.R.color.transparent)
                    .toDrawable()
        }
    }
}