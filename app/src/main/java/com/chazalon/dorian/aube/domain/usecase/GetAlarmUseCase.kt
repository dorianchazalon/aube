package com.chazalon.dorian.aube.domain.usecase

import com.chazalon.dorian.aube.domain.model.Alarm
import com.chazalon.dorian.aube.domain.repository.AlarmRepository


class GetAlarmUseCase(private val alarmRepository: AlarmRepository) {

    suspend fun getAllAlarms(): List<Alarm> {
        return alarmRepository.allAlarms()
    }

    suspend fun getAlarm(id: Int): Alarm {
        return alarmRepository.alarm(id)
    }
}