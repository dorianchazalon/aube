package com.chazalon.dorian.aube.domain.model


data class MusicState(
    val artist: String,
    val song: String
)