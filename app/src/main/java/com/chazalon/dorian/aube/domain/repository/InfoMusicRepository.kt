package com.chazalon.dorian.aube.domain.repository

import com.chazalon.dorian.aube.domain.model.Playlist


interface InfoMusicRepository {
    suspend fun connect()

    suspend fun playlists(): List<Playlist>
}